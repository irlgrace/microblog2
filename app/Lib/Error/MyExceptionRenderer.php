<?php
App::uses('ExceptionRenderer', 'Error');

class MyExceptionRenderer extends ExceptionRenderer {

  protected function _outputMessage($template) {
    $this->controller->layout = 'ajax';
    parent::_outputMessage($template);
  }

}