<nav class="navbar navbar-dark navbar-expand-lg bg-dark justify-content-between">
    <div class="container">
        <?php echo $this->Html->link(
                'MICROBLOG 2',
                [
                    'controller' => 'users',
                    'action' => 'register',
                    'full_base' => true
                ],
                [
                    'class' => 'navbar-brand',
                    'style' => 'color:#ffffff;'
                ]
            );
        ?>
        <?php echo $this->Form->create('User', [
                'url' => ['controller' => 'users', 'action' => 'login'],
                'id' => 'UsersLogin',
                'class' => 'form-inline'
            ]); 
            echo $this->Form->input('username',
                ['label' => false,
                    'class' => 'form-control',
                    'type' => 'text',
                    'placeholder' => 'Username',
                    'error' => false
                ]
            );
        ?>
        &nbsp;
        <?php
            echo $this->Form->input('password',
                ['label' => false,
                    'class' => 'form-control',
                    'type' => 'password',
                    'placeholder' => 'Password',
                    'error' => false
                ]
            );
        ?>
        &nbsp;
        <?php  
            echo $this->Form->end([
                    'label' => 'Login',
                    'class' => 'btn btn-primary',
                ]
            );  
        ?>
    </div>
</nav>