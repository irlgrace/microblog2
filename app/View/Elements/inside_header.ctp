<nav class="navbar navbar-dark navbar-expand-lg bg-dark justify-content-between fixed-top">
    <div class="container">
        <?php echo $this->Html->link(
            'MICROBLOG 2',
            [
                'controller' => 'posts',
                'action' => 'index',
                'full_base' => true
            ],
            [
                'class' => 'navbar-brand',
                'style' => 'color:#ffffff;'
            ]
        );
        ?>
        <?php
        echo $this->Form->create(
            false,
            [
                'url' => ['controller' => 'users', 'action' => 'searchUser'],
                'id' => 'UserSearch',
                'class' => 'form-inline',
                'type' => 'get'
            ],
        );
        echo $this->Form->input('search', [
            'label' => false,
            'div' => false,
            'class' => 'form-control mr-sm2',
            'type' => 'search',
            'placeholder' => 'Search',
            'value' => isset($search)?$search:'',
        ]);
        echo '&nbsp;';
        echo $this->Form->end([
            'div' => false,
            'label' => 'Search',
            'class' => 'btn btn-primary my-2 my-sm-0',
            'type' => 'submit'
        ]);
        ?>
        &nbsp;
        <div class='border border-secondary'>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                        <?php echo $this->Html->image(
                            empty($me['image']) ? 'user.jpg' : h($me['image']),
                            [
                                'alt' => 'You',
                                'style' => 'border-radius:50%; width: 30px;'
                            ]
                        );
                        echo '&nbsp';
                        echo h($me['username']);
                        ?> <span class="navbar-toggler-icon"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <?php
                        echo $this->Html->link(
                            'My UserPage',
                            [
                                'controller' => 'users',
                                'action' => 'userPage',
                                h($me['id'])
                            ],
                            ['class' => 'dropdown-item']
                        );
                        echo $this->Html->link(
                            'Account Settings',
                            [
                                'controller' => 'users',
                                'action' => 'accountSettings',
                                'full_base' => true,
                            ],
                            ['class' => 'dropdown-item']
                        );
                        echo $this->Html->link(
                            'Logout',
                            [
                                'controller' => 'users',
                                'action' => 'logout',
                                'full_base' => true,
                            ],
                            ['class' => 'dropdown-item']
                        );
                        ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>