<ul class="list-group list-group-flush">
    <?php
    foreach ($followers as $follower) {
        echo '<li class="list-group-item">' .
            '<div class="div_pic_name">' .
            $this->Html->image(
                empty($follower['User']['image']) ? 'user.jpg' : h($follower['User']['image']),
                [
                    'alt' => 'You',
                    'style' => 'border-radius:50%; width: 30px;'
                ]
            ) .
            '&nbsp;' .
            '<h5>' . $this->Html->link(
                h($follower['User']['username']),
                ['controller' => 'users', 'action' => 'userPage', h($follower['User']['id'])],
            ) . '</h5>' .
            '&nbsp;' .
            '<small><em>' . h($follower['User']['email']) . '</em></small>' .
            '</div>' .
            '</li>';
    }
    ?>
</ul>