<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="card">
            <div class="card-body">
                <input type="hidden" id="user_id" value="<?= h($user['User']['id']) ?>" />
                <h4><?php
                    echo $this->Html->link(
                        'Followers',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowers',
                            h($user['User']['id'])
                        ],
                        ['style' => 'color:blue']
                    );
                    echo ' | ';
                    echo $this->Html->link(
                        'Following',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowings',
                            h($user['User']['id'])
                        ],
                    );
                    ?>
                </h4>
                <hr />
                <div class="card">
                    <div id="load_data"></div>
                </div>
                <div id="load_data_message"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var url = getFollowerUrl();

    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            user_id: getUser()
        }
    }
</script>