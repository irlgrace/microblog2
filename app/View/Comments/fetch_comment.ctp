<div class="comment-content" id="page<?=$page?>" style="display:block;">
    <?php foreach ($comments as $comment) { ?>
        <div class="post-comments">
            <?php if ($me['id'] == $comment['Comment']['user_id']) { ?>
                <div class="float-right">
                    <div class="dropleft">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            ...
                        </a>
                        <div class="dropdown-menu">
                            <?php
                            echo $this->Html->link(
                                'Edit',
                                ['controller' => 'comments', 'action' => 'edit', h($comment['Comment']['id'])],
                                ['class' => 'edit_comment_link dropdown-item']
                            );
                            echo $this->Form->postLink(
                                'Delete',
                                ['controller' => 'comments', 'action' => 'delete', h($comment['Comment']['id'])],
                                [
                                    'confirm' => 'Are you sure to delete?',
                                    'class' => 'dropdown-item'
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php echo $this->Html->image(
                empty($comment['User']['image']) ? 'user.jpg' : h($comment['User']['image']),
                [
                    'class' => 'profile'
                ]
            );
            echo '&nbsp;';
            echo $this->Html->link(
                h($comment['User']['username']),
                ['controller' => 'users', 'action' => 'userPage', h($comment['Comment']['user_id'])],
            );
            ?>
            <p><?=h($comment['Comment']['comment'])?></p>
        </div>
    <?php } ?>
</div>