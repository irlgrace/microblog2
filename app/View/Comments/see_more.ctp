<div class='row'>
    <div class='col-md-5 col-lg-5'>
        <div class='div_post position-fixed card' style='width:30%'>
            <!--start-->
            <div class='div-post-upper'>
                <div class='div_pic_name'>
                    <?php echo $this->Html->image(
                        empty($post['User']['image']) ? 'user.jpg' : h($post['User']['image']),
                        [
                            'class' => 'profile'
                        ]
                    );
                    echo '&nbsp;';
                    echo '<h6>' . $this->Html->link(
                        h($post['User']['username']),
                        ['controller' => 'users', 'action' => 'userPage', h($post['Post']['user_id'])],
                    ) . '</h6>';
                    ?>
                </div>
                <p><?= h($post['Post']['post']) ?></p>
                <?php
                if ($post['Post']['post_image'] != null) {
                    echo '<div class="card bg-secondary">';
                    echo $this->Html->image(
                        h($post['Post']['post_image']),
                        [
                            'class' => 'center post_img',
                            'alt' => h($post['Post']['post'])
                        ]
                    );
                    echo '</div>';
                }
                ?>
                <?php if ($post['Post']['retweeted_post_id'] != null) { ?>
                    <a href="<?php echo $this->webroot . 'posts/view/' . h($post['Post']['retweeted_post_id']); ?>" class='post-view-link'>
                        <div class="div_post">
                            <?php if ($post['RetweetedPost']['deleted']) {
                                echo '<p class="text-danger">The post has been removed.</p>';
                            } else {
                                echo '<div class="div_pic_name">';
                                echo $this->Html->image(
                                    empty($post['RetweetedPost']['User']['image'])
                                        ? 'user.jpg'
                                        : h($post['RetweetedPost']['User']['image']),
                                    [
                                        'class' => 'profile'
                                    ]
                                );
                                echo '&nbsp;';
                                echo '<h6>' . h($post['RetweetedPost']['User']['username']) . '</h6>';
                                echo '</div>';
                                echo '<p>' . h($post['RetweetedPost']['post']) . '</p>';
                                if ($post['RetweetedPost']['post_image'] != null) {
                                    echo '<div class="card bg-secondary">';
                                    echo $this->Html->image(
                                        h($post['RetweetedPost']['post_image']),
                                        [
                                            'class' => 'center post_img',
                                            'alt' => h($post['RetweetedPost']['post'])
                                        ]
                                    );
                                    echo '</div>';
                                }
                            }
                            ?>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <div>
                <a href='#'>
                    <small id="postcount<?= h($post['Post']['id']) ?>">
                        <?php
                        if ($post['Post']['like_count'] > 0) {
                            echo $post['Post']['like_count'] > 1 ?
                                h($post['Post']['like_count']) . ' Likes' :
                                h($post['Post']['like_count']) . ' Like';
                        }
                        ?>
                    </small>
                </a>
            </div>
            <div class='d-flex justify-content-between'>
                <div class='float-left'>
                    <?php if (empty($post['Like'])) {
                        echo '<a href="#" class="like-post" data-post="' . h($post['Post']['id']) . '">Like</a>';
                    } else {
                        if ($post['Like'][0]['deleted']) {
                            echo '<a href="#" class="like-post" data-post="' . h($post['Post']['id']) . '">Like</a>';
                        } else {
                            echo '<a href="#" class="like-post" data-post="' . h($post['Post']['id']) . '">Unlike</a>';
                        }
                    } ?>
                </div>
                <div class='float-right'>
                    <div class='dropdown'>
                        <button class='dropbtn'>...</button>
                        <div class='dropdown-content'>
                            <?php if ($me['id'] == $post['Post']['user_id']) {
                                echo $this->Html->link(
                                    'Edit',
                                    ['controller' => 'posts', 'action' => 'edit', h($post['Post']['id'])],
                                    ['class' => 'edit_post_link']
                                );
                                echo $this->Form->postLink(
                                    'Delete',
                                    ['controller' => 'posts', 'action' => 'delete', h($post['Post']['id'])],
                                    ['confirm' => 'Are you sure to delete?']
                                );
                            }
                            echo $this->Html->link(
                                'Retweet',
                                ['controller' => 'posts', 'action' => 'retweet', h($post['Post']['id'])],
                                ['class' => 'retweet_post_link']
                            );
                            echo $this->Html->link(
                                'View',
                                ['controller' => 'posts', 'action' => 'view', h($post['Post']['id'])],
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END OF COL_MD_%-->
    <div class='col-md-7 col-lg-7'>
        <div class='div_post'>
            <div id='load_data'></div>
            <div id='load_data_message'></div>
            <div class='div_comment_form'>
                <div>
                    <?php echo $this->Html->image(
                        empty($me['image']) ? 'user.jpg' : h($me['image']),
                        [
                            'alt' => 'You',
                            'class' => 'comment_form_img'
                        ]
                    );
                    ?>
                </div>
                <div>
                    &nbsp;
                    <?php
                    echo $this->Html->link(
                        h($me['username']),
                        ['controller' => 'users', 'action' => 'userPage', $me['id']],
                    );
                    echo $this->Form->create('Comment', [
                        'url' => array('controller' => 'comments', 'action' => 'add'),
                        'class' => 'comment-form',
                    ]);
                    ?>
                    <?php echo $this->Form->input('comment', [
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'style' => 'width:450px',
                    ]);

                    echo $this->Form->input('post_id', [
                        'label' => false,
                        'type' => 'hidden',
                        'value' => h($post['Post']['id']),
                        'id' => 'post_id'
                    ]);

                    echo '&nbsp';
                    echo $this->Form->end(array(
                        'label' => 'Comment',
                        'class' => 'btn-sm btn-success',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    init(false);
    var url = getCommentUrl();
    var data = function(limit, page) {
        return {
            post_id: getPost(),
            limit: limit,
            page: page,
        }
    }
</script>