<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $title; ?></title>
<!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">-->
<!-- Include external files and scripts here (See HTML helper for more info.) -->
<?php
    echo $this->Html->css(['bootstrap.min', 'inside_design']);
    echo $this->Html->script(['jquery-3.4.1.min', 'popper.min', 'bootstrap.min','inside_js']); 
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
?>
</head>
<body>
<?php 
    echo $this->element('inside_header');
?>

<div class='fixed container' style="margin-top: 100px">
    <div class="row">
        <div class="col-md-7 col-lg-7 offset-md-5 offset-lg-5">
        <?php echo $this->Flash->render();
            if(isset($validations)) {
                if ($validations != '') {
                    echo '
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">' .
                        $validations . '
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>';
                }
            }
        ?>
        </div>
    </div>
    <?php
        echo $this->fetch('content');
        echo $this->element('edit_modal');
    ?>
</div>
<?php echo $this->Html->script('inside'); ?>
</body>
</html>