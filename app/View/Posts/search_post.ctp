<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class='card'>
            <div class='card-body'>
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>
                        <div class='float-left'>
                            <input type='hidden' id='search_word' value='<?= h($search) ?>' />
                            <h4>Search Results</h4>
                        </div>
                        <div class='float-right d-flex flex-row'>
                            <div class='user_search_div'>
                                <?php
                                echo $this->Form->create(
                                    false,
                                    [
                                        'url' => ['controller' => 'users', 'action' => 'searchUser'],
                                        'id' => 'UserSearch',
                                        'type' => 'get'
                                    ],
                                );
                                echo $this->Form->input('search', [
                                    'label' => false,
                                    'div' => false,
                                    'class' => 'form-control mr-sm2',
                                    'type' => 'hidden',
                                    'value' => h($search)
                                ]);
                                echo $this->Form->end([
                                    'div' => false,
                                    'label' => 'User',
                                    'class' => 'btn btn-primary w-100',
                                    'type' => 'submit'
                                ]);
                                ?>
                            </div>
                            &nbsp;
                            <div class='post_search_div'>
                                <?php
                                echo $this->Form->create(
                                    false,
                                    [
                                        'url' => ['controller' => 'posts', 'action' => 'searchPost'],
                                        'id' => 'UserSearch',
                                        'type' => 'get'
                                    ],
                                );
                                echo $this->Form->input('search', [
                                    'label' => false,
                                    'div' => false,
                                    'class' => 'form-control mr-sm2',
                                    'type' => 'hidden',
                                    'value' => h($search)
                                ]);
                                echo $this->Form->end([
                                    'div' => false,
                                    'label' => 'Post',
                                    'class' => 'btn btn-primary w-100 active',
                                    'type' => 'submit'
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>

                        <div id='load_data'></div>

                        <div id='load_data_message'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var url = getSearchUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            search: getSearchWord()
        }
    }
</script>