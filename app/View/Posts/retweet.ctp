<?php if (!isset($error)) { ?>
    <?php
    echo $this->Form->create('Post', [
        'url' => array('controller' => 'posts', 'action' => 'retweetPost'),
        'id' => 'PostsRetweet'
    ]);
    ?>

    <div class='input-group'>
        <?php echo $this->Form->input('post', [
            'label' => false,
            'div' => false,
            'id' => 'input-post',
            'class' => 'form-control',
            'type' => 'textarea',
            'rows' => '2',
            'placeholder' => 'Say something...'
        ]);
        ?>
    </div>
    <div class='div_post'>
        <!--start-->
        <div class='div_pic_name'>
            <?php echo $this->Html->image(
                empty($post['User']['image']) ? 'user.jpg' : $post['User']['image'],
                [
                    'style' => 'border-radius:50%; width: 30px;'
                ]
            );
            echo '&nbsp;';
            echo '<h6>' . $this->Html->link(
                h($post['User']['username']),
                ['controller' => 'users', 'action' => 'userPage', h($post['Post']['user_id'])],
            ) . '</h6>';
            ?>
        </div>
        <p><?= h($post['Post']['post']) ?></p>
        <?php
        if ($post['Post']['post_image'] != null) {
            echo '<div class="card bg-secondary">';
            echo $this->Html->image(
                h($post['Post']['post_image']),
                [
                    'style' => 'width: 200px;',
                    'class' => 'center'
                ]
            );
            echo '</div>';
        }
        ?>
    </div>
    <!--end-->
    <br />
    <div class='float-right'>
        <div class='input-group' id='button_post_div'>
            <input type='text' id='input-post-count' disabled size='4' />
            &nbsp;
            <?php echo $this->Form->input('retweeted_post_id', [
                'label' => false,
                'type' => 'hidden',
                'value' => h($post['Post']['id'])
            ]);
            ?>
            <?php echo $this->Form->end([
                'label' => 'Post',
                'class' => 'btn-sm btn-success',
            ]);
            ?>
        </div>
    </div>
<?php } ?>