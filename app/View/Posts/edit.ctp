<?php
if (!isset($error)) {
?>
    <?php echo $this->Form->create('Post', [
        'url' => array('controller' => 'posts', 'action' => 'editPost'),
        'id' => 'PostsEdit',
        'type' => 'file'
    ]);
    ?>
    <?php echo $this->Form->input('post', [
        'label' => false,
        'id' => 'input-post',
        'class' => 'form-control',
        'type' => 'textarea',
    ]);
    ?>

    <div class='float-left'>
        <div id='button_post_div'>
            <?php
            if ($this->request->data['RetweetedPost']['id'] == null) {
                echo $this->Form->input('post_image', [
                    'label' => false,
                    'type' => 'file'
                ]);
            }
            if ($this->request->data['Post']['post_image'] != null) {
                echo $this->Form->input('remove_image', [
                    'label' => 'Remove Image',
                    'type' => 'checkbox',
                ]);
            }
            ?>
        </div>
    </div>
    <div class="float-right">
        <div class="input-group" id="button_post_div">
            <input type="text" id="input-post-count" disabled size="4" />
            &nbsp;
            <?php echo $this->Form->end([
                'label' => 'Post',
                'class' => 'btn-sm btn-success',
            ]);
            ?>
        </div>
    </div>
<?php } ?>