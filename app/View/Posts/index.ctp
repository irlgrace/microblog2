<div id='content' class='row'>
    <div class='col-md-5 col-sm-5 col-lg-5'>
        <div class='card bg-secondary text-center' id='me_card' style='position: fixed; width: 25%; height: 75%;'>
            <div class='card-body'>
                <?php echo $this->Html->image(empty($me['image']) ? 'user.jpg' : h($me['image']), [
                    'alt' => 'You',
                    'style' => 'border-radius:50%; width: 150px; margin-top:30%'
                ]); ?>
                <br />
                <div class='line'></div>
                <h5 class='card-title'>
                    <?php echo $this->Html->link(
                        h($me['username']),
                        [
                            'controller' => 'users',
                            'action' => 'userPage', h($me['id'])
                        ],
                        [
                            'class' => 'white',
                        ]
                    );
                    ?>
                </h5>
                <p class='white'><?= h($me['email']) ?></p>
            </div>
        </div>
    </div>
    <div class='col-md-7 col-sm-7 col-lg-7'>
        <div id='div_post_input' class='card'>
            <?php echo $this->Form->create('Post', [
                'url' => array('controller' => 'posts', 'action' => 'create'),
                'id' => 'PostsCreate',
                'type' => 'file'
            ]);
            ?>

            <div class='input-group'>
                <?php echo $this->Form->input('post', [
                    'label' => false,
                    'div' => false,
                    'id' => 'input-post',
                    'class' => 'form-control',
                    'type' => 'textarea',
                    'rows' => '2',
                    'placeholder' => 'Say something...'
                ]);
                ?>
            </div>
            <div class='float-left'>
                <div id='button_post_div'>
                    <?php echo $this->Form->input('post_image', [
                            'label' => false,
                            'type' => 'file'
                        ]);
                    ?>
                </div>
            </div>
            <div class='float-right'>
                <div class='input-group' id='button_post_div'>
                    <input type='text' id='input-post-count' disabled size='4' />
                    &nbsp;
                    <?php echo $this->Form->end([
                        'label' => 'Post',
                        'class' => 'btn-sm btn-success',
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div id='load_data'></div>
        <div id='load_data_message'></div>
    </div>
</div>
<!--end of content-->
<script>
    var url = getPostUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
        }
    }
</script>