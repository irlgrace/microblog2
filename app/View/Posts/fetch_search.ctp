<!--start-->
<?php foreach ($posts as $post) { ?>
    <div class='div_post'>
        <div class='div-post-upper'>
            <div class='div_pic_name'>
                <?php echo $this->Html->image(
                    empty($post['User']['image']) ? 'user.jpg' : h($post['User']['image']),
                    [
                        'class' => 'profile'
                    ]
                );
                echo '&nbsp';
                echo '<h6>' . $this->Html->link(
                    h($post['User']['username']),
                    ['controller' => 'users', 'action' => 'userPage', h($post['Post']['user_id'])],
                ) . '</h6>';
                ?>
            </div>
            <p><?= h($post['Post']['post']) ?></p>
            <?php
            if ($post['Post']['post_image'] != null) {
                echo '<div class="card bg-secondary">';
                echo $this->Html->image(
                    h($post['Post']['post_image']),
                    [
                        'class' => 'center post_img',
                        'alt' => h($post['Post']['post'])
                    ]
                );
                echo '</div>';
            }
            ?>
            <?php if ($post['Post']['retweeted_post_id'] != null) { ?>
                <a href="<?php echo $this->webroot . 'posts/view/' . h($post['Post']['retweeted_post_id']); ?>" class='post-view-link'>
                    <div class='div_post card'>
                        <?php if ($post['RetweetedPost']['deleted']) {
                            echo '<p class="text-danger">The post has been removed.</p>';
                        } else {
                            echo '<div class="div_pic_name">';
                            echo $this->Html->image(
                                empty($post['RetweetedPost']['User']['image'])
                                    ? 'user.jpg'
                                    : h($post['RetweetedPost']['User']['image']),
                                [
                                    'class' => 'profile'
                                ]
                            );
                            echo '&nbsp;';
                            echo '<h6>' . h($post['RetweetedPost']['User']['username']) . '</h6>';
                            echo '</div>';
                            echo '<p>' . h($post['RetweetedPost']['post']) . '</p>';
                            if($post['RetweetedPost']['post_image'] != null) {
                                echo '<div class="card bg-secondary">';
                                echo $this->Html->image(
                                    h($post['RetweetedPost']['post_image']),
                                    [
                                        'class' => 'center post_img',
                                        'alt' => h($post['RetweetedPost']['post'])
                                    ]
                                ); 
                                echo '</div>';
                            }
                        }
                        ?>
                    </div>
                </a>
            <?php } ?>
        </div>
        <div class='row'>
            <div class='col-md-12 col-lg-12'>
                <div class='float-left d-flex flex-row'>
                    <a href='#'>
                        <p id="postcount<?= h($post['Post']['id']) ?>">
                            <?php echo $post['Post']['like_count'] > 1 ?
                                h($post['Post']['like_count']) . ' Likes' :
                                h($post['Post']['like_count']) . ' Like';
                            ?>
                        </p>
                    </a> &nbsp;
                    <?php
                    echo $this->Html->link(
                        'Comment',
                        ['controller' => 'comments', 'action' => 'seeMore', h($post['Post']['id'])],
                    );
                    ?>
                </div>
                <div class='float-right'>
                    <div class='dropdown'>
                        <button class='dropbtn'>...</button>
                        <div class='dropdown-content'>
                            <?php
                            echo $this->Html->link(
                                'View',
                                ['controller' => 'posts', 'action' => 'view', h($post['Post']['id'])],
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!--end-->