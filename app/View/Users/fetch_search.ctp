<?php                              
    foreach ($users as $user) {
        echo '<div class="div_post">'.
                '<div class="div_pic_name">'.
                    $this->Html->image(
                        empty($user['User']['image'])?'user.jpg':h($user['User']['image']), 
                    [
                        'class' => 'profile'
                    ]).
                    '&nbsp;'.
                    '<h5>'.$this->Html->link(
                        h($user['User']['username']),
                        ['controller' => 'users','action' => 'userPage', h($user['User']['id'])],
                    ).'</h5>'.
                    '&nbsp;'.
                    '<small><em>'.h($user['User']['email']).'</em></small>'.
                '</div>'.
                '</div>';   
    } 
?>