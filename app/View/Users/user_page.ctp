<div class="row">
    <div class="col-md-5 col-lg-5">
        <div id="userSide">
            <div class="card text-center bg-secondary per_card" id="me_card">
                <div class="card-body">
                    <?php echo $this->Html->image(empty($user['User']['image']) ? 'user.jpg' : h($user['User']['image']), [
                        'alt' => 'You',
                        'style' => 'border-radius:50%; width: 150px; margin-top:10%'
                    ]); ?>
                    <br />
                    <h5 class="card-title">
                        <?php echo $this->Html->link(
                            h($user['User']['username']),
                            [
                                'controller' => 'users',
                                'action' => 'userPage', h($user['User']['id'])
                            ],
                            [
                                'class' => 'white'
                            ]
                        );
                        ?>
                    </h5>
                    <p class="white"><?= h($user['User']['email']) ?></p>
                    <input type="hidden" value="<?= h($user['User']['id']) ?>" id="user_id" />
                </div>
            </div>
            <div class="card text-center per_card shadow">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item" id="follower_following_count">
                        <?php
                        echo $user['User']['follower_count'] > 1 ? h($user['User']['follower_count']) . ' Followers ' : h($user['User']['follower_count']) . ' Follower ';
                        echo $user['User']['following_count'] > 1 ? h($user['User']['following_count']) . ' Followings' : h($user['User']['following_count']) . ' Following';
                        ?>
                    </li>
                </ul>
            </div>
            <div class="card per_card shadow">
                <div class="card-header text-center bg-secondary">
                    <?php
                    echo $this->Html->link(
                        'List of Followers',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowers',
                            h($user['User']['id'])
                        ],
                        [
                            'class' => 'white'
                        ]
                    );
                    ?>
                </div>
                <ul class="list-group list-group-flush" id="followers_ul">
                    <?php
                    if (!$user['Follower']) {
                        echo '<li class="list-group-item text-center">' .
                            'No Followers...' .
                            '</li>';
                    } else {
                        foreach ($user['Follower'] as $follower) {
                            echo '<li class="list-group-item">';
                            echo $this->Html->image(
                                empty($follower['User']['image']) ? 'user.jpg' : h($follower['User']['image']),
                                [
                                    'alt' => 'You',
                                    'style' => 'border-radius:50%; width: 30px;'
                                ]
                            );
                            echo '&nbsp;';
                            echo $this->Html->link(
                                h($follower['User']['username']),
                                [
                                    'controller' => 'users',
                                    'action' => 'userPage', h($follower['User']['id'])
                                ],
                            );
                            echo '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="card per_card shadow">
                <div class="card-header text-center bg-secondary">
                    <?php
                    echo $this->Html->link(
                        'List of Following',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowings',
                            h($user['User']['id'])
                        ],
                        [
                            'class' => 'white'
                        ]
                    );
                    ?>
                </div>
                <ul class="list-group list-group-flush">
                    <?php
                    if (!$user['Following']) {
                        echo '<li class="list-group-item text-center">' .
                            'No Followings...' .
                            '</li>';
                    } else {
                        foreach ($user['Following'] as $following) {
                            echo '<li class="list-group-item">';
                            echo $this->Html->image(
                                empty($following['FollowedUser']['image']) ? 'user.jpg' : h($following['FollowedUser']['image']),
                                [
                                    'alt' => 'You',
                                    'style' => 'border-radius:50%; width: 30px;'
                                ]
                            );
                            echo '&nbsp;';
                            echo $this->Html->link(
                                h($following['FollowedUser']['username']),
                                [
                                    'controller' => 'users',
                                    'action' => 'userPage', h($following['FollowedUser']['id'])
                                ],
                            );
                            echo '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>

    </div>

    <div class="col-md-7 col-sm-7 col-lg-7">
        <div class="div_follow">
            <?php
            if ($me['id'] != $user['User']['id']) {
                if ($user['User']['followed']) {
                    echo '<a href="#" class="btn btn-primary follow_btn" data-user="' . h($user['User']['id']) . '">Unfollow</a>';
                } else {
                    echo '<a href="#" class="btn btn-primary follow_btn" data-user="' . h($user['User']['id']) . '">Follow</a>';
                }
            }
            ?>
        </div>
        <div>
            <?php if ($me['id'] == $user['User']['id']) { ?>
                <div id="div_post_input" class="card">
                    <?php echo $this->Form->create('Post', [
                        'url' => ['controller' => 'posts', 'action' => 'create'],
                        'id' => 'PostsCreate',
                        'type' => 'file'
                    ]);
                    ?>
                    <div class="input-group">
                        <?php echo $this->Form->input('post', [
                            'label' => false,
                            'div' => false,
                            'id' => 'input-post',
                            'class' => 'form-control',
                            'type' => 'textarea',
                            'rows' => '2',
                            'placeholder' => 'Say something...'
                        ]);
                        ?>
                    </div>
                    <div class='float-left'>
                        <div id='button_post_div'>
                            <?php echo $this->Form->input('post_image', [
                                    'label' => false,
                                    'type' => 'file'
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="float-right">
                        <div class="input-group" id="button_post_div">
                            <input type="text" id="input-post-count" disabled size="4" />
                            &nbsp;
                            <?php echo $this->Form->end([
                                'label' => 'Post',
                                'class' => 'btn-sm btn-success',
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="load_data"></div>
            <div id="load_data_message"></div>
        </div>
    </div>
</div>
<!--end of content -->
<script>
    var url = getUserPagePostUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            user_id: getUser()
        }
    }

    includeFollow();
</script>