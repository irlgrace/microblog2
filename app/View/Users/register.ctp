<div class="card shadow cream" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Register User</h5>
        <?php
        if(isset($user)) {
            echo $this->Form->create('User', [
                'url' => ['controller' => 'users', 'action' => 'resendMail'],
                'id' => 'ResendMail'
            ]);
            echo $this->Form->input(
                'username',
                [
                    'type' => 'hidden',
                    'value' => $user['username']
                ]
            );
            echo $this->Form->input(
                'email',
                [
                    'type' => 'hidden',
                    'value' => $user['email']
                ]
            );
            echo $this->Form->input(
                'activation_code',
                [
                    'type' => 'hidden',
                    'value' => $user['activation_code']
                ]
            );
            echo $this->Form->end(
                [
                    'label' => 'Resend Email Verification',
                    'class' => 'btn-sm btn-warning',
                ]
            );    
        }
        echo $this->Flash->render();
        echo $this->Form->create('User', [
            'url' => ['controller' => 'users', 'action' => 'register'],
            'id' => 'UsersRegister'
        ]);
        echo $this->Form->input(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );
        echo $this->Form->input(
            'password',
            [
                'label' => 'Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->input(
            'confirm_password',
            [
                'label' => 'Confirm Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->input(
            'email',
            [
                'label' => 'Email:',
                'class' => 'form-control-custom',
                'type' => 'email'
            ]
        );
        echo $this->Form->end(
            [
                'label' => 'Register',
                'class' => 'form-control-custom success-custom',
            ]
        );

        ?>
    </div>
</div>