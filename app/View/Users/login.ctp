<div class="card shadow cream" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Login User</h5>
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->Flash->render('auth'); ?>

        <?php echo $this->Form->create(
            'User',
            [
                'url' => ['controller' => 'users', 'action' => 'login'],
                'id' => 'UsersLogin'
            ]
        ); ?>
        <?php
        echo $this->Form->input(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );

        echo $this->Form->input(
            'password',
            [
                'label' => 'Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->end(
            [
                'label' => 'Login',
                'class' => 'form-control-custom success-custom',
            ]
        );

        ?>
    </div>
</div>