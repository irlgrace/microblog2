<div class="card shadow cream" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Activate User</h5>
        <?php
        echo $this->Flash->render();
        echo $this->Form->create('User', [
            'url' => ['controller' => 'users', 'action' => 'activate'],
            'id' => 'UsersActivate'
        ]);

        echo $this->Form->input(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );

        echo $this->Form->input(
            'activation_code',
            [
                'label' => 'Activation Code:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );
        echo $this->Form->end(
            [
                'label' => 'Activate',
                'class' => 'form-control-custom success-custom',
            ]
        );

        ?>
    </div>
</div>