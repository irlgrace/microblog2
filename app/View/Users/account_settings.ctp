<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="card">
            <div class="card-body">
                <h3>MyAccount Settings</h3>
                <hr />
            </div>
            <div class="row" style="margin:40px 0px;">
                <div class="col-md-6 col-lg-6 text-center">
                    <?php echo $this->Html->image(empty($me['image']) ? 'user.jpg' : h($me['image']), [
                        'alt' => 'You',
                        'style' => 'border-radius:50%; width: 130px; margin:20px;'
                    ]); ?>

                    <br />
                    <div id="editImagePasswordButton">
                        <button style="margin: 10px; width: 200px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#editUserImage">
                            Edit Image
                        </button>
                        <br />
                        <button style="margin: 10px; width: 200px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#editPassword">
                            Change Password
                        </button>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6" style="padding-right:80px;">
                    <h5>Username & Email</h5>
                    <?php echo $this->Form->create('User', [
                        'url' => ['controller' => 'users', 'action' => 'saveUsernameAndEmail'],
                        'id' => 'UsersSaveUsernameAndEmail'
                    ]);
                    ?>
                    <div class="form-group">
                        <?php echo $this->Form->input(
                            'username',
                            [
                                'label' => 'Username:',
                                'class' => 'form-control',
                                'type' => 'text',
                                'value' => h($me['username']),
                            ]
                        );
                        ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->input(
                            'email',
                            [
                                'label' => 'Email:',
                                'class' => 'form-control',
                                'type' => 'email',
                                'value' => h($me['email']),
                            ]
                        );
                        ?>
                    </div>
                    <?php echo $this->Form->end([
                        'label' => 'Save Username and Email',
                        'class' => 'form-control btn btn-success',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- User Image Modal -->
<div class="modal fade" id="editUserImage" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserImageTitle">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create(false, [
                    'url' => ['controller' => 'users', 'action' => 'saveImage'],
                    'id' => 'UserSaveImage',
                    'type' => 'file'
                ]);
                ?>
                <div class="form-group">
                    <?php echo $this->Form->input('image', [
                        'label' => 'User Image :',
                        'class' => 'form-control-file',
                        'type' => 'file'
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end([
                    'label' => 'Save Image',
                    'class' => 'btn btn-success',
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<!-- User Password Modal -->
<div class="modal fade" id="editPassword" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserPasswordTitle">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('User', [
                    'url' => array('controller' => 'users', 'action' => 'savePassword'),
                    'id' => 'UsersSavePassword'
                ]);
                ?>
                <div class="form-group">
                    <?php echo $this->Form->input('password', [
                        'label' => 'Password :',
                        'class' => 'form-control',
                        'type' => 'password'
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('confirm_password', [
                        'label' => 'Confirm Password :',
                        'class' => 'form-control',
                        'type' => 'password'
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end([
                    'label' => 'Save Password',
                    'class' => 'form-control btn btn-success',
                ]);
                ?>
            </div>
        </div>
    </div>
</div>