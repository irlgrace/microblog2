<?php
App::uses('AppModel', 'Model');
class Post extends AppModel
{
    public $actsAs = ['Containable'];

    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'RetweetedPost' => [
            'className' => 'Post',
            'foreignKey' => 'retweeted_post_id'
        ]
    ];
    public $hasMany = [
        'Comment' => [
            'className' => 'Comment',
            'foreignKey' => 'post_id',
            'conditions' => ['Comment.deleted' => false],
            'order' => 'Comment.created DESC',
            'limit' => 3,
            'dependent' => true
        ],
        'Like' => [
            'className' => 'Like',
            'foreignKey' => 'post_id',
            'conditions' => ['Like.deleted' => false],
            'limit' => 1,
            'dependent' => true
        ]
    ];
    public $validate = [
        'post' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Ops. You forgot to say something.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 140],
                'message' => 'Post must be not more than 140 characters.'
            ],
        ],
        
    ];

    /**
     * Check if post is own by user
     *  
     * @return bool 
     * @param $post, $user
     */
    public function isOwnedBy($post, $user)
    {

        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }

    /**
     * Check if post is exiting and not deleted 
     *  
     * @return bool 
     * @param $post
     */
    public function isInvalid($post)
    {
        if ($this->hasAny(['Post.id' => $post])) {
            $deleted = $this->read('deleted', $post);
            return $deleted['Post']['deleted'];
        }
        return true;
    }

    /**
     * Function for Retrieving Post 
     *  
     * @return $posts array 
     * @param $limit, $page, $user_id
     */
    public function fetchPost($limit, $page, $user_id, $conditions)
    {
        $posts = $this->find(
            'all',
            [
                'conditions' => [
                    'OR' => $conditions,
                    'Post.deleted' => false
                ],
                'order' => ['Post.created' => 'desc'],
                'limit' => $limit,
                'page' => $page,
                'contain' => [
                    'User.username',
                    'User.image',
                    'RetweetedPost.post',
                    'RetweetedPost.deleted',
                    'RetweetedPost.post_image',
                    'RetweetedPost.User',
                    'Comment.id',
                    'Comment.comment',
                    'Comment.User',
                    'Like.user_id =' . $user_id,
                ],
            ]
        );
        return $posts;
    }

    /**
     * Function for Retrieving Post of a specific user 
     *  
     * @return $posts array 
     * @param $limit, $page, $user_id
     */
    public function fetchUserPost($limit, $page, $user_id, $auth_user)
    {
        $posts = $this->find(
            'all',
            [
                'conditions' => ['Post.deleted' => false, 'Post.user_id' => $user_id],
                'order' => ['Post.created' => 'desc'],
                'limit' => $limit,
                'page' => $page,
                'contain' => [
                    'User.username',
                    'User.image',
                    'RetweetedPost.post',
                    'RetweetedPost.deleted',
                    'RetweetedPost.post_image',
                    'RetweetedPost.User',
                    'Comment.id',
                    'Comment.comment',
                    'Comment.User',
                    'Like.user_id =' . $auth_user,
                ],
            ]
        );
        return $posts;
    }

    /**
     * Function for searching post 
     *  
     * @return $posts array 
     * @param $word, $limit, $page
     */
    public function searchPost($word, $limit, $page)
    {
        $posts = $this->find(
            'all',
            [
                'fields' => [
                    'Post.id',
                    'Post.post',
                    'Post.user_id',
                    'Post.retweeted_post_id',
                    'Post.post_image',
                    'Post.created',
                    'Post.like_count'
                ],
                'limit' => $limit,
                'page' => $page,
                'order' => ['Post.created' => 'desc'],
                'contain' => [
                    'User.username',
                    'User.image',
                    'RetweetedPost.post',
                    'RetweetedPost.deleted',
                    'RetweetedPost.post_image',
                    'RetweetedPost.User'
                ],
                'conditions' => [
                    'OR' => [
                        [   
                            'Post.post LIKE' => '%' . $word . '%',
                            'Post.deleted' => false,
                        ],                        
                        [
                            'RetweetedPost.post LIKE' => '%' . $word . '%',
                            'RetweetedPost.deleted' => false,
                        ]
                    ],
                    'Post.deleted' => false
                ],
            ],
        );

        return $posts;
    }
}
