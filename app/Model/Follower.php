<?php
App::uses('AppModel', 'Model');
class Follower extends AppModel
{
	public $actsAs = ['Containable'];
	public $belongsTo = [
		'User' =>  [
			'className' => 'User',
			'foreignKey' => 'user_id',
			'counterCache' => 'following_count',
			'counterScope' => [
				'Follower.deleted' => false
			]
		],
		'FollowedUser' => [
			'className' => 'User',
			'foreignKey' => 'following_user_id',
			'counterCache' => 'follower_count',
			'counterScope' => [
				'Follower.deleted' => false
			]
		]
	];
	public $validate = [];

	/**
	 * Function for Retrieving Follower 
	 *  
	 * @return $followers array 
	 * @param $limit, $page, $user_id
	 */
	public function fetchFollowers($limit, $page, $user_id)
	{
		$followers = $this->find(
			'all',
			[
				'conditions' => [
					'Follower.deleted' => false,
					'Follower.following_user_id' => $user_id
				],
				'order' => ['Follower.modified' => 'desc'],
				'limit' => $limit,
				'page' => $page,
				'contain' => ['User'],
			]
		);
		return $followers;
	}

	/**
	 * Function for Retrieving Follower 
	 *  
	 * @return $followings array 
	 * @param $limit, $page, $user_id
	 */
	public function fetchFollowings($limit, $page, $user_id)
	{
		$followings = $this->find(
			'all',
			[
				'conditions' => [
					'Follower.deleted' => false,
					'Follower.user_id' => $user_id
				],
				'order' => ['Follower.modified' => 'desc'],
				'limit' => $limit,
				'page' => $page,
				'contain' => ['FollowedUser'],
			]
		);
		return $followings;
	}
}
