<?php
App::uses('AppModel', 'Model');
class Like extends AppModel
{
    public $actsAs = ['Containable'];

    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'counterCache' => true,
            'counterScope' => array(
                'Like.deleted' => false
            )
        ]
    ];

    /**
     * Check if like is own by user
     *  
     * @return bool 
     * @param $like, $user
     */
    public function isOwnedBy($like, $user)
    {

        return $this->field('id', array('id' => $like, 'user_id' => $user)) !== false;
    }
}
