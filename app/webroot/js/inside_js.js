//For Pagination Variable
var limit = 10;
var page = 1;
var loadingAction = 'inactive';

//Getting the webroot
var webroot = function () {
   $hostname = window.location.hostname;
   if ($hostname == 'localhost') {
        return '/microblog2/';
   } else {
        return '/';
   }
   
}

var getPost = function () {
    return document.querySelector('#post_id').value;
}

var getUser = function () {
    return document.querySelector('#user_id').value;
}

var getSearchWord = function () {
    return document.querySelector('#search_word').value;
}

//Getting Url
var getPostUrl = function () {
    return webroot() + "posts/fetchPost";
}

var getUserPagePostUrl = function () {
    return webroot() + "users/fetchUserPost";
}

var getCommentUrl = function () {
    return webroot() + "comments/fetchComment";
}

var getFollowerUrl = function () {
    return webroot() + "followers/fetchFollowers";
}

var getFollowingUrl = function () {
    return webroot() + "followers/fetchFollowings";
}

var getSearchUrl = function () {
    return webroot() + "posts/fetchSearch";
}

var getUserSearchUrl = function () {
    return webroot() + "users/fetchSearch";
}

//Include to Page Function
function init(include) {
    if(include) {
        //for comment
        var coll = document.querySelectorAll(".comment-collapsible");
        comment(coll);
        var editCommentLink = document.querySelectorAll(".edit_comment_link");
        editComment(editCommentLink);
    }
    //for post
    var likesData = document.querySelectorAll(".like-post");
    like(likesData);
    var editPostLink = document.querySelectorAll(".edit_post_link");
    editPost(editPostLink);
    var retweetLink = document.querySelectorAll(".retweet_post_link");
    retweetPost(retweetLink);
}

//Only call in userpage
function includeFollow() {
    var foll = document.querySelector('.follow_btn');
    if (foll != null) {
        follow(foll);
    }
}

/*========Logical Functions with Ajax========*/

//for show comment list
function comment(coll) {
    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function (e) {
            e.preventDefault();
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }
}

//for like and unlike function
function like(like) {
    like.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var post_id = this.dataset.post;
                var el = this;
                var url = webroot() + "likes/like";
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        post_id: post_id,
                    },
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            var likeVar = JSON.parse(data);
                            if (likeVar.deleted) {
                                el.innerHTML = 'Like';
                            } else {
                                el.innerHTML = 'Unlike';
                            }
                            if (likeVar.like_count > 1) {
                                document.querySelector('#postcount' + post_id).innerHTML = likeVar.like_count + ' Likes';
                            } else if (likeVar.like_count == 1) {
                                document.querySelector('#postcount' + post_id).innerHTML = likeVar.like_count + ' Like';
                            } else {
                                document.querySelector('#postcount' + post_id).innerHTML = '';
                            }
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                    }

                });
            });
        }
    );
}

// For follow a person
function follow(foll) {
    foll.addEventListener('click', function (e) {
        e.preventDefault();
        var user_id = this.dataset.user;
        var el = this;
        var url = webroot() + 'followers/follow';
        var root = webroot() + '';
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                user_id: user_id
            },
            cache: false,
            success: function (data) {

                if (data == null || data == '') {
                    location.reload();
                } else {
                    var followVar = JSON.parse(data);
                    if (followVar.deleted) {
                        el.innerHTML = 'Follow';
                    } else {
                        el.innerHTML = 'Unfollow';
                    }

                    var follower_following_count = document.querySelector('#follower_following_count');
                    var followStr = '';
                    if (followVar['follower_count'] > 1) {
                        followStr += followVar['follower_count'] + ' Followers ';
                    } else {
                        followStr += followVar['follower_count'] + ' Follower ';
                    }

                    if (followVar['following_count'] > 1) {
                        followStr += followVar['following_count'] + ' Followings';
                    } else {
                        followStr += followVar['following_count'] + ' Following';
                    }
                    follower_following_count.innerHTML = followStr;
                    fillFollowers(root, followVar['FollowerList']);
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }

        });
    });
}

//to update the follower list as user followe certain person
function fillFollowers(root, followers) {
    var followDiv = '';
    var count = 0;
    if (followers.length < 1) {
        followDiv = followDiv + '<li class="list-group-item text-center"> No Followers...</li>'

    } else {

        for (count = 0; count < followers.length; count++) { 
            var image = '';
            if (followers[count]['User']['image'] === null) {
                image = root + 'img/user.jpg';
            } else {
                if (root.length > 1) {
                    image = root + followers[count]['User']['image'];
                } else {
                    image = followers[count]['User']['image'];
                }
                
            }
            followDiv = followDiv + '<li class="list-group-item">' +
                '<img alt="You" style="border-radius:50%; width: 30px;" ' +
                'src="' + image + '"/> &nbsp; ' +
                '<a href="' + root + 'users/userPage/' + followers[count]['User']['id'] + '">' +
                followers[count]['User']['username'] + '</a>'
                + '</li>';
        }
    }
    document.querySelector('#followers_ul').innerHTML = followDiv;
}

//For some delay in loading data
function lazzyLoader() {
    var output = '';
    output += '<div class="text-center" style="margin-top: 10px">';
    output += '<img src="' + webroot() + 'img/loader.gif" width="100"/>';
    output += '</div>';
    $('#load_data_message').html(output);
}

//For Loading data for the page through scroll pagination
//data and url varies in every page
function loadData(data, url) {
    var page = data.page;
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        cache: false,
        success: function (data) {
            if (data.length < 80 || data == '') {
                $('#load_data_message').html('<a href="#"><p>No More Result Found...</p></a>');
                loadingAction = 'active';
            } else {
                $('#load_data').append(data);
                $('#load_data_message').html("");
                loadingAction = 'inactive';
            }

            //To determine of for comment
            var parent = document.querySelector('#page' + page);
            if (parent != null) {
                var editCommentLink = parent.querySelectorAll(".edit_comment_link");
                if (editCommentLink != null) {
                    editComment(editCommentLink);
                }
            }
            //To determine if for Post
            var parentForPost = document.querySelector('#post' + page);
            if (parentForPost != null) {
                var retweetLink = document.querySelector('#post' + page).querySelectorAll(".retweet_post_link");
                retweetPost(retweetLink);
                var editPostLink = document.querySelector('#post' + page).querySelectorAll(".edit_post_link");
                editPost(editPostLink);
                var editCommentLink = document.querySelector('#post' + page).querySelectorAll(".edit_comment_link");
                editComment(editCommentLink);
                var coll = document.querySelector('#post' + page).querySelectorAll(".comment-collapsible");
                comment(coll);
                var data = document.querySelector('#post' + page).querySelectorAll(".like-post");
                like(data);
            }
        },
        error: function (xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText
            console.log('Error - ' + errorMessage);
        }
    });
}

//For Edit Post Modal
function editPost(ePost) {
    ePost.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Edit Post';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            console.log(el);
                            showMaxChar(parentEl, el);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                    }

                });
            });
        }
    );
}

//For Edit Comment Modal
function editComment(eComment) {
    eComment.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Edit Comment';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            showMaxChar(parentEl, el);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                    }
                });
            });
        }
    );
}

//For Retweet Post Modal
function retweetPost(rPost) {
    rPost.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Retweet Post';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            showMaxChar(parentEl, el);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                    }

                });
            });
        }
    );
}

//For Showing the Max Character For Every Input Inside a ajax request
function showMaxChar(parent, input_post) {
    var maxCharLimit = 140;

    if (input_post != null) {
        parent.querySelector("#input-post-count").value = input_post.value.length + '/' + maxCharLimit;
        input_post.addEventListener("keyup", function (e) {
            var lengthCount = this.value.length;
            if (lengthCount == maxCharLimit) {
                e.preventDefault();
            }
            parent.querySelector("#input-post-count").value = lengthCount + '/' + maxCharLimit;
        });
    }
}