<?php
class FollowersController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, [
            'follow',
            'viewFollowers',
            'viewFollowings',
            'fetchFollowers',
            'fetchFollowings'
        ])) {
            return true;
        }
    }

    /**
     * Set layout before filter 
     *  
     * @return bool 
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'inside';
        $this->Security->unlockedActions = [
            'follow',
            'fetchFollowers',
            'fetchFollowings'
        ];
        return true;
    }

    /**
     * Follow or unfollow a user
     * 
     * @return json response or null
     */
    public function follow()
    {

        if ($this->request->is(['post'])) {
            $this->autoRender = false;

            if (isset($this->request->data['user_id'])) {
                //Check if user follow itself
                if ($this->Auth->user('id') == $this->request->data['user_id']) {
                    return null;
                }

                $this->loadModel('User');

                //Check if user going to follow is existing and not deleted
                if ($this->User->isInvalid($this->request->data['user_id'])) {
                    return null;
                }

                $conditions = [
                    'Follower.user_id' => $this->Auth->user('id'),
                    'Follower.following_user_id' => $this->request->data['user_id']
                ];
                $saved = false;

                //Check if there is existing record
                if ($this->Follower->hasAny($conditions)) {
                    $this->Follower->contain();
                    $follower = $this->Follower->find('first', ['conditions' => $conditions]);
                    if ($follower['Follower']['deleted']) {
                        $follower['Follower']['deleted'] = false;
                    } else {
                        $follower['Follower']['deleted'] = true;
                    }

                    if ($this->Follower->save($follower)) {
                        $saved = true;
                    }
                } else {
                    //Create new one if none
                    $this->Follower->create();
                    $follower['Follower']['user_id'] = $this->Auth->user('id');
                    $follower['Follower']['following_user_id'] = $this->request->data['user_id'];
                    $follower['Follower']['deleted'] = false;
                    if ($this->Follower->save($follower)) {
                        $saved = true;
                    }
                }

                if ($saved) { //if saved successfully
                    $this->User->contain();

                    //Getting updated follower and following count of users to follow
                    $user = $this->User->find('first', [
                        'conditions' => ['id' => $this->request->data['user_id']]
                    ]);
                    $follower['Follower']['follower_count'] = $user['User']['follower_count'];
                    $follower['Follower']['following_count'] = $user['User']['following_count'];

                    //Getting the updated list of first 5 recent follower
                    $this->Follower->contain('User.image', 'User.username', 'User.id');
                    $followerList = $this->Follower->fetchFollowers(5, 1, $this->request->data['user_id']);
                    $follower['Follower']['FollowerList'] = $followerList;

                    return $this->json($follower['Follower']);
                }
            }
            
            return null;
        }
    }

    /**
     * Function for View of List of Follower
     * 
     * @return view or redirected page
     * @throws NotFoundException
     * @param $id of user
     */
    public function viewFollowers($id)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No user reference'));
            }

            $this->loadModel('User');
            if ($this->User->isInvalid($id)) {
                throw new NotFoundException(__('Invalid User'));
            }

            $user = $this->User->find('first', ['conditions' => ['User.id' => $id]]);

            if (!$user) {
                throw new NotFoundException(__('Getting Follower List of User does not exist'));
            } else if ($user['User']['deleted']) {
                throw new NotFoundException(__('Getting Follower List of User does not exist'));
            }
            $this->set(['user' => $user]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving List of Follower
     * 
     * @return ajax view
     */
    public function fetchFollowers()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['limit']) &&
                isset($this->request->data['page']) &&
                isset($this->request->data['user_id'])
            ) {
                $followers = $this->Follower->fetchFollowers(
                    $this->request->data['limit'],
                    $this->request->data['page'],
                    $this->request->data['user_id'],
                );

                $this->set(['followers' => $followers]);
            }
        }
    }

    /**
     * Function for View of List of Following
     * 
     * @return view or redirected page
     * @throws NotFoundException
     * @param $id of user
     */
    public function viewFollowings($id)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No user reference...'));
            }

            $this->loadModel('User');

            if ($this->User->isInvalid($id)) {
                throw new NotFoundException(__('Invalid User'));
            }

            $user = $this->User->find('first', ['conditions' => ['User.id' => $id]]);

            if (!$user) {
                throw new NotFoundException(__('Getting Following List of User does not exist'));
            } else if ($user['User']['deleted']) {
                throw new NotFoundException(__('Getting Following List of User does not exist'));
            }
            $this->set(['user' => $user]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving List of Following
     * 
     * @return ajax view
     */
    public function fetchFollowings()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['limit']) &&
                isset($this->request->data['page']) &&
                isset($this->request->data['user_id'])
            ) {
                $followings = $this->Follower->fetchFollowings(
                    $this->request->data['limit'],
                    $this->request->data['page'],
                    $this->request->data['user_id'],
                );

                $this->set(['followings' => $followings]);
            }
        }
    }
}
