<?php
class PostsController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool or @return redirected page
     */
    public function isAuthorized($user)
    {
        //Anyone that are login can access this action
        if (in_array($this->action, [
            'create',
            'view',
            'retweet',
            'retweetPost',
            'index',
            'fetchPost',
            'searchPost',
            'fetchSearch',
            'edit'
        ])) {
            return true;
        }

        /*If the post is owned by authenticated user, 
        it can access the action otherwise return to previous page*/
        if (in_array($this->action, ['editPost', 'delete'])) {

            $postId = (int) $this->request->params['pass'][0];

            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            } else {
                $this->Flash->error(__('Request not Authorized'));
                return $this->redirect(Router::url($this->referer(), true));
            }
        }
    }

    /**
     * Set layout before filter 
     *  
     * @return bool or @return redirected page
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'inside';

        $this->Security->unlockedActions = ['fetchPost', 'fetchSearch'];

        //In this actions, check if it is accessing a existing and not deleted post
        if (in_array($this->action, ['editPost', 'delete'])) {

            //Check if accessing the right url with parameter
            if (empty($this->request->params['pass'])) {
                if ($this->action == 'edit') {
                    return null;
                }
                $this->Flash->error(__('No post referencing'));
                return $this->redirect(Router::url($this->referer(), true));
            }

            $postId = (int) $this->request->params['pass'][0];

            if ($this->Post->isInvalid($postId)) {
                if ($this->action == 'edit') {
                    return null;
                }
                $this->Flash->error(__('Post does not exist'));
                return $this->redirect(Router::url($this->referer(), true));
            }
        }
    }

    /**
     * Index Page of the posts / data here will be render in ajax  
     *  
     * @return view
     */
    public function index()
    {
    }

    /**
     * Function that will render in post through ajax  
     *  
     * @return ajax view
     */
    public function fetchPost()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {

            $this->loadModel('Follower');
            $this->Follower->contain();
            $followedUser = $this->Follower->find('all', [
                'conditions' => [
                    'Follower.deleted' => false,
                    'Follower.user_id' => $this->Auth->user('id')
                ]
            ]);

            $conditions = [];

            foreach ($followedUser as $followed) {
                $conditions[] = ['Post.user_id' => $followed['Follower']['following_user_id']];
            }

            $conditions[] =  ['Post.user_id' => $this->Auth->user('id')];

            if (isset($this->request->data['limit']) && isset($this->request->data['page'])) {
                $posts = $this->Post->fetchPost(
                    $this->request->data['limit'],
                    $this->request->data['page'],
                    $this->Auth->user('id'),
                    $conditions
                );

                $this->set(['posts' => $posts, 'page' => $this->request->data['page']]);
            }
        }
    }

    /**
     * 
     * Function for viewing one specific post  
     * @param id or null
     * @return view or redirected page
     */
    public function view($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No post referencing'));
            }

            if ($this->Post->isInvalid($id)) { //Check if post is existing and not deleted
                throw new NotFoundException(__('Post does not exist'));
            }

            $this->Post->contain(
                [
                    'User.username',
                    'User.image',
                    'RetweetedPost.post',
                    'RetweetedPost.deleted',
                    'RetweetedPost.post_image',
                    'RetweetedPost.User',
                    'Comment',
                    'Comment.User',
                    'Like.user_id =' . $this->Auth->user('id'),
                ]
            );

            $post = $this->Post->findById($id);

            $this->set('post', $post);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Adding post
     * 
     * @return redirected page
     */
    public function create()
    {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->request->data['Post']['post'] = trim($this->request->data['Post']['post']);
            $image = $this->request->data['Post']['post_image'];
            unset($this->request->data['Post']['post_image']);

            if (isset($image) && $image['name'] != null) {
                if ($image['error'] != 0) {
                    $this->Flash->error(__("Error occured while uploading the file."));
                    return $this->redirect(Router::url($this->referer(), true));
                }
                $fileTmpPath = $image['tmp_name'];
                $fileName = $image['name'];
                $fileSize = $image['size'];

                $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
                $newFileName = md5($this->Auth->user('id') . uniqid()) . '.' . $fileExtension;

                $this->imageValidate($fileSize, $fileExtension);

                $uploadFileDir = WWW_ROOT . '/img/post/';
                $destPath = $uploadFileDir . $newFileName;

                $this->request->data['Post']['post_image'] = '/img/post/' . $newFileName;
            }

            if ($this->Post->save($this->request->data)) {
                if (isset($this->request->data['Post']['post_image'])) {
                    move_uploaded_file($fileTmpPath, $destPath);
                }
                $this->Flash->success(__('Successfully created a post.'));
            } else {
                /*Set a session named PostValidation if an error occured upon saving
                This is retrieve in AppController beforeRender function*/
                $this->Session->write('PostValidation', $this->Post->validationErrors);
            }
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for validating Image
     * 
     * @return redirected page or boolean
     */
    public function imageValidate($size, $extension)
    {

        $allowedExtensions = ['jpg', 'jpeg', 'png'];

        //Allow file to upload with image extension
        if (in_array($extension, $allowedExtensions)) {

            //File size should be not more than 2MB
            if ($size < 2000000) {
                return true;
            } else {
                $this->Flash->error(__("Image is too large, 2MB is the limit size of image."));
                return $this->redirect(Router::url($this->referer(), true));
            }
        } else {
            $this->Flash->error(__("Invalid File Format for Image. Only .jpg, .jpeg and .png are allowed."));
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving Editing Post Form
     * 
     * @return ajax view or null
     * @throws NotFoundException
     * @param $id or null
     */
    public function edit($id = null)
    {
        $this->layout = 'ajax';
        try {
            if (!$id) {
                throw new NotFoundException(__('Invalid post'));
            }

            if ($this->Post->isInvalid($id)) {
                throw new NotFoundException(__('Invalid post'));
            }

            if (!$this->Post->isOwnedBy($id, $this->Auth->user('id'))) {
                throw new NotFoundException(__('Invalid Request'));
            }

            $post = $this->Post->findById($id);
            if (!$post) {
                throw new NotFoundException(__('Invalid post'));
            }
            if (!$this->request->data) {
                $this->request->data = $post;
            }
        } catch (NotFoundException $e) {
            $this->set('error', $e->getMessage());
        }
    }

    /**
     * Function for Editing Post submitted through post or put 
     * 
     * @return redirected page with flash message
     * @throws NotFoundException
     * @param $id or null
     */
    public function editPost($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('Invalid post'));
            }

            $this->autoRender = false;

            if ($this->request->is(['post', 'put'])) {
                //For Image
                $image = $this->request->data['Post']['post_image'];
                unset($this->request->data['Post']['post_image']);
                
                //Edit Image
                if (isset($image) && $image['name'] != null) {
                    if ($image['error'] != 0) {
                        $this->Flash->error(__("Error occured while uploading the file."));
                        return $this->redirect(Router::url($this->referer(), true));
                    }
                    $fileTmpPath = $image['tmp_name'];
                    $fileName = $image['name'];
                    $fileSize = $image['size'];
                    $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
                    $newFileName = md5($this->Auth->user('id') . uniqid()) . '.' . $fileExtension;

                    $this->imageValidate($fileSize, $fileExtension);

                    $uploadFileDir = WWW_ROOT . '/img/post/';
                    $destPath = $uploadFileDir . $newFileName;

                    $this->request->data['Post']['post_image'] = '/img/post/' . $newFileName;

                    $this->Post->read(['post', 'post_image'], $id);
                    $this->Post->set([
                        'post' => trim($this->request->data['Post']['post']),
                        'post_image' => $this->request->data['Post']['post_image']
                    ]);
                } else {
                    $this->Post->read('post', $id);
                    $this->Post->set(['post' => trim($this->request->data['Post']['post'])]);
                }

                //Remove Image
                if (
                    isset($this->request->data['Post']['remove_image']) &&
                    $this->request->data['Post']['remove_image'] == true
                ) {
                    $this->request->data['Post']['post_image'] = null;

                    $this->Post->read(['post', 'post_image'], $id);
                    $this->Post->set([
                        'post' => trim($this->request->data['Post']['post']),
                        'post_image' => $this->request->data['Post']['post_image']
                    ]);
                }

                if ($this->Post->save()) {
                    if (
                        isset($this->request->data['Post']['post_image']) &&
                        $this->request->data['Post']['post_image'] != null
                    ) {
                        move_uploaded_file($fileTmpPath, $destPath);
                    }
                    $this->Flash->success(__('Post has been updated.'));
                } else {
                    /*Set a session named PostValidation if an error occured upon saving
                    This is retrieve in AppController beforeRender function*/
                    $this->Session->write('PostValidation', $this->Post->validationErrors);
                }
            }
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
        } finally {
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Soft deleting a post
     * 
     * @return redirected page with flash message
     * @throws MethodNotAllowedException
     * @param $id
     */
    public function delete($id)
    {
        $this->authRender = false;
        try {
            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $this->Post->read(['deleted', 'deleted_date'], $id);
            $this->Post->set([
                'deleted_date' => date('Y-m-d H:i:s'),
                'deleted' => true
            ]);

            if ($this->Post->save()) {
                $this->Flash->success(
                    __('Successfully deleted a post!')
                );
            } else {
                $this->Flash->error(
                    __('Error while deleting a post.')
                );
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(
                __('Access not authorized.')
            );
        } finally {
            $url = Router::parse($this->referer('/', true));
            //These routes will not accept deleted post causing redirect infinitely
            //To avoid it redirect to index page
            if (($url['controller'] == 'posts' && $url['action'] == 'view') ||
                ($url['controller'] == 'comments' && $url['action'] == 'seeMore')
            ) {
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retweeting Post
     * 
     * @return ajax view or null
     * @throws NotFoundException
     * @param $id 
     */
    public function retweet($id = null)
    {
        try {
            $this->layout = 'ajax';
            if (!$id) {
                throw new NotFoundException(__('No post referencing'));
            }

            if ($this->Post->isInvalid($id)) {
                throw new NotFoundException(__('Post does not exist'));
            }

            $post = $this->Post->findById($id);
            if (!$post) {
                throw new NotFoundException(__('Invalid post'));
            }
            $this->set('post', $post);
        } catch (NotFoundException $e) {
            $this->set('error', $e->getMessage());
        }
    }

    /**
     * Function for Retweeting Post submit data
     * 
     * @return redirected page
     * @throws NotFoundException
     * @param $id 
     */
    public function retweetPost()
    {
        if ($this->request->is(['post', 'put'])) {
            if ($this->Post->isInvalid($this->request->data['Post']['retweeted_post_id'])) {
                $this->Flash->error(__('Post does not exist'));
            } else {
                $this->Post->create();
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                $this->request->data['Post']['post'] = trim($this->request->data['Post']['post']);
                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Successfully retweeted a post.'));
                } else {
                    /*Set a session named PostValidation if an error occured upon saving
                    This is retrieve in AppController beforeRender function*/
                    $this->Session->write('PostValidation', $this->Post->validationErrors);
                }
            }
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving Search View
     * 
     * @return view
     */
    public function searchPost()
    {
        $this->layout = 'inside';
        if ($this->request->is(['get'])) {
            $searchWord = $this->request->query('search');
            $this->set(['search' => $searchWord]);
        }
    }

    /**
     * Function for Retrieving the data for Search 
     * 
     * @return ajax view
     */
    public function fetchSearch()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['limit']) &&
                isset($this->request->data['page']) &&
                isset($this->request->data['search'])
            ) {
                $posts = $this->Post->searchPost(
                    $this->request->data['search'],
                    $this->request->data['limit'],
                    $this->request->data['page'],
                );

                $this->set(['posts' => $posts]);
            }
        }
    }
}
