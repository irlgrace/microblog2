<?php
App::uses('CakeEmail', 'Network/Email');
class UsersController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {   //All login user can access this controller
        return true;
    }

    /**
     * Set layout before filter 
     *  
     * @return bool or @return redirected page
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        //Unlocked Action for Ajax return
        $this->Security->unlockedActions = ['fetchUserPost', 'fetchSearch'];

        //Allowing not login user to access this routes
        $this->Auth->allow('register', 'resendMail', 'activate', 'activateUser', 'logout');

        /*If user is logged in user will redirect to specify url in 
        AppController Components for loggedIn User*/
        if (in_array($this->action, ['register', 'activate', 'login'])) {
            if ($this->Auth->loggedIn()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    /**
     * Function for Login 
     *  
     * @return 0 or @return Auth redirected page
     */
    public function login()
    {
        $this->layout = 'front';
        if ($this->request->is('post')) {

            $conditionsForDelete = [
                'User.username' => $this->request->data['User']['username'],
                'User.deleted' => true
            ];

            $conditions = [
                'User.username' => $this->request->data['User']['username'],
                'User.activated' => false
            ];

            if ($this->User->hasAny($conditionsForDelete)) {
                $this->Flash->error(__('Invalid username or password, try again'));
                return 0;
            }

            if ($this->User->hasAny($conditions)) {
                $this->Flash->error(__('Cannot Login. User not activated'));
                return 0;
            }
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    /**
     * Function for Logout
     *  
     * @return Auth redirected page
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Function for User Registration
     * 
     * @return CakeResponse
     */
    public function register()
    {
        $this->layout = 'front';
        if ($this->request->is('post')) {

            //Creating Activation Code
            $activation_code = substr(md5(microtime()), rand(0, 26), 8);
            $this->request->data['User']['activation_code'] =  $activation_code;

            if ($this->User->save($this->request->data)) {

                //After saving send email
                if ($this->sendMail($this->request->data)) {
                    $this->Flash->success(__("Successfully Registered! Please check your email for account's activation code."));
                    return $this->redirect([
                        'action' => 'register'
                    ]);
                } else {
                    $this->Flash->error(__('Registration complete but email failed to send...contact admin'));
                    $this->set(['user' => $this->request->data['User']]);
                }
            } else {
                $this->Flash->error(__('Unable to register user.'));
            }
        }
    }

    /**
     * Function for sending email
     * 
     * @return bool
     * @param $data
     */
    public function sendMail($data)
    {
        try {
            $linkServer = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $activationLink = $this->webroot . 'users/activate/' . $data['User']['username'];
            $link = $this->webroot . 'users/activateUser/' . $data['User']['username'];
            $Email = new CakeEmail();
            $Email->template('default', 'verification_layout');
            $Email->emailFormat('html');
            $Email->config('gmail');
            $Email->to($data['User']['email']);
            $Email->from(['microblog-grace@gmail.com' => 'Microblog 2']);
            $Email->viewVars([
                'name' =>  $data['User']['username'],
                'code' => $data['User']['activation_code'],
                'link' =>  $linkServer . $activationLink,
                'verification_link' => $linkServer . $link . '/' . $data['User']['activation_code']
            ]);
            $Email->subject('Activate Account');

            return $Email->send();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for Resending email
     * 
     * return CakeResponse
     */
    public function resendMail()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if ($this->sendMail($this->request->data)) {
                $this->Flash->success(__("Successfully Send to Email. Please activate your account."));
            } else {
                $this->Flash->error(__("Email still not send due to error, Please contact the admin to verify your account."));
            }
            return $this->redirect([
                'action' => 'register'
            ]);
        }
    }

    /**
     * Function for Manual User activation
     * 
     * @return CakeResponse
     */
    public function activate($username = null)
    {
        $this->layout = 'front';
        if ($this->request->is('post')) {
            $conditions = [
                'User.username' => $this->request->data['User']['username'],
            ];

            //Check if username is existing
            if ($this->User->hasAny($conditions)) {
                $user = $this->User->find(
                    'first',
                    [
                        'conditions' => $conditions,
                        'contains' => false
                    ]
                );

                //Check if the activation code is same with the database
                if ($user['User']['activation_code'] == $this->request->data['User']['activation_code']) {
                    $this->User->contain();
                    $user = $this->User->read('activated', $user['User']['id']);
                    if ($user['User']['activated']) {
                        $this->Flash->error(__("User already activated."));
                    } else {
                        $this->User->set('activated', true);
                        if ($this->User->save()) {
                            $this->Flash->success(__("Successfully Activated the Account! You may now use the site by logging in"));
                        } else {
                            $this->Flash->error(__('Unable to activate the account.'));
                        }
                    }
                }
            }
        }
    }

    /**
     * Function for User Link Activation
     * 
     * @return CakeResponse
     * @param $username, $activationCode
     */
    public function activateUser($username, $activationCode)
    {
        $this->autoRender = false;
        $conditions = [
            'User.username' => $username,
            'User.activation_code' => $activationCode
        ];

        if ($this->User->hasAny($conditions)) {
            $user = $this->User->find(
                'first',
                [
                    'conditions' => $conditions,
                    'contains' => false
                ]
            );
            $this->User->read('activated', $user['User']['id']);
            //If already activated
            if ($user['User']['activated']) {
                $user = $this->User->find(
                    'first',
                    [
                        'conditions' => $conditions,
                        'contains' => false
                    ]
                );
                if ($this->Auth->login($user['User'])) {
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('Unable to login.'));
                }
            }
            //Not yet activated
            $this->User->set('activated', true);
            if ($this->User->save()) {
                $this->Flash->success(__("Successfully Activated the Account! Welcome to Microblog2."));
                $user = $this->User->find(
                    'first',
                    [
                        'conditions' => $conditions,
                        'contains' => false
                    ]
                );
                if ($this->Auth->login($user['User'])) {
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('Unable to login.'));
                }
            } else {
                $this->Flash->error(__('Unable to activate the account.'));
            }
        } else {
            $this->Flash->error(__('Activating Not Existing User...'));
        }
        return $this->redirect([
            'action' => 'register'
        ]);
    }

    /**
     * Function for View of Account Settings
     * 
     * @return view
     */
    public function accountSettings()
    {
        $this->layout = 'inside';
    }

    /**
     * Function for Changing Password
     * 
     * @return redirect Page
     */
    public function savePassword()
    {
        if ($this->request->is(['post', 'put'])) {
            $this->User->contain();
            $this->User->read(null, $this->Auth->user('id'));
            $this->User->set([
                'password' => $this->request->data['User']['password'],
                'confirm_password' => $this->request->data['User']['confirm_password']
            ]);
            if ($this->User->save()) {
                $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                $this->Flash->success(__("Successfully Saved the Password"));
            } else {
                /*Set a session named PasswordValidation if an error occured upon saving
                This is retrieve in AppController beforeRender function*/
                $this->Session->write('PasswordValidation', $this->User->validationErrors);
            }
            return $this->redirect([
                'action' => 'accountSettings'
            ]);
        }
    }

    /**
     * Function for Changing Username and Email
     * 
     * @return redirect Page
     */
    public function saveUsernameAndEmail()
    {
        if ($this->request->is(['post', 'put'])) {
            $this->User->contain();
            $this->User->read(['username', 'email'], $this->Auth->user('id'));
            $this->User->set([
                'username' => $this->request->data['User']['username'],
                'email' => $this->request->data['User']['email']
            ]);
            if ($this->User->save()) {
                //Rewrite the existing Auth
                $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                $this->Flash->success(__("Successfully Saved Username and Email"));
            } else {
                /*Set a session named UsernameAndEmailValidation if an error occured upon saving
                This is retrieve in AppController beforeRender function*/
                $this->Session->write('UsernameAndEmailValidation', $this->User->validationErrors);
            }
            return $this->redirect([
                'action' => 'accountSettings'
            ]);
        }
    }

    /**
     * Function for Changing Image for User
     * 
     * @return redirect Page
     */
    public function saveImage()
    {
        if ($this->request->is(['post', 'put'])) {

            if (isset($this->request->data['image']) && $this->request->data['image']['name'] != '') {
                //Getting Image info
                if ($this->request->data['image']['error'] != 0) {
                    $this->Flash->error(__("Error occured while uploading the file."));
                    return $this->redirect(Router::url($this->referer(), true));
                }
                $fileTmpPath = $this->request->data['image']['tmp_name'];
                $fileName = $this->request->data['image']['name'];
                $fileSize = $this->request->data['image']['size'];
                $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
                $newFileName = md5($this->Auth->user('id')) . '.' . $fileExtension;

                $allowedExtensions = ['jpg', 'jpeg', 'png'];

                //Allow file to upload with image extension
                if (in_array($fileExtension, $allowedExtensions)) {

                    //File size should be not more than 2MB
                    if ($fileSize < 2000000) {
                        $uploadFileDir = WWW_ROOT . '/img/user/';
                        $destPath = $uploadFileDir . $newFileName;

                        $this->User->contain();
                        $oldImage = $this->User->read('image', $this->Auth->user('id'));
                        $this->User->set([
                            'image' => '/img/user/' . $newFileName,
                        ]);

                        if ($this->User->save()) {
                            move_uploaded_file($fileTmpPath, $destPath);
                            //Rewrite Auth User
                            $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));

                            //delete old image
                            if (
                                $oldImage['User']['image'] != '/img/user/' . $newFileName &&
                                $oldImage['User']['image'] != null
                            ) {
                                unlink(WWW_ROOT . $oldImage['User']['image']);
                            }
                            $this->Flash->success(__("Successfully save the account's Image"));
                        }
                    } else {
                        $this->Flash->error(__($fileSize . "Image is too large, 2MB is the limit size of image."));
                    }
                } else {
                    $this->Flash->error(__("Invalid File Format for Image. Only .jpg, .jpeg, .png are allowed."));
                }
            } else {
                $this->Flash->error(__("Please Select an Image."));
            }

            return $this->redirect([
                'action' => 'accountSettings'
            ]);
        }
    }

    /**
     * Function for Rendering User Page
     * 
     * @return view or redirect Page
     * @param id or null
     */
    public function userPage($id = null)
    {
        try {
            $this->layout = 'inside';
            if (!$id) {
                throw new NotFoundException(__('No reference user'));
            }

            if ($this->User->isInvalid($id)) {
                throw new NotFoundException(__('Invalid user'));
            }
            //Getting the List of Follower and List of Following
            $this->User->contain(['Follower.User', 'Following.FollowedUser']);
            $user = $this->User->find('first', [
                'conditions' => ['User.id' => $id]
            ]);

            if (!$user) {
                throw new NotFoundException(__('Invalid user'));
            } else if ($user['User']['deleted']) {
                throw new NotFoundException(__('Invalid user'));
            }

            //Checking if authenticated user followed the user
            $followed = true;
            $this->loadModel('Follower');
            $this->Follower->contain();
            $follower = $this->Follower->find(
                'first',
                ['conditions' => [
                    'Follower.user_id' => $this->Auth->user('id'),
                    'Follower.following_user_id' => $id
                ]]
            );

            if (!$follower) {
                $followed = false;
            } else if ($follower['Follower']['deleted']) {
                $followed = false;
            }

            $user['User']['followed'] = $followed;
            $this->set('user', $user);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect([
                'controller' => 'posts',
                'action' => 'index'
            ]);
        }
    }

    /**
     * Function for Retrieving User Post for User Page
     * 
     * @return ajax view
     */
    public function fetchUserPost()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['limit']) &&
                isset($this->request->data['page']) &&
                isset($this->request->data['user_id'])
            ) {
                $this->loadModel('Post');
                $posts = $this->Post->fetchUserPost(
                    $this->request->data['limit'],
                    $this->request->data['page'],
                    $this->request->data['user_id'],
                    $this->Auth->user('id')
                );
                $this->set(['posts' => $posts, 'page' => $this->request->data['page']]);
            }
        }
    }

    /**
     * Function for Rendering Search User View
     * 
     * @return view
     */
    public function searchUser()
    {
        $this->layout = 'inside';
        if ($this->request->is(['get'])) {
            $searchWord = $this->request->query('search');
            $this->set(['search' => $searchWord]);
        }
    }

    /**
     * Function for Retrieving User Information for Search User View
     * 
     * @return ajax view
     */
    public function fetchSearch()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['search']) &&
                isset($this->request->data['limit']) &&
                isset($this->request->data['page'])
            ) {
                $users = $this->User->searchUser(
                    $this->request->data['search'],
                    $this->request->data['limit'],
                    $this->request->data['page'],
                );
                $this->set(['users' => $users]);
            }
        }
    }
}
