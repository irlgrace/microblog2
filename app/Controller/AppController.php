<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Set $helpers variable defaults
     * @var array
     */
    public $helpers = ['Html', 'Form', 'Flash'];

    /**
     * Set $components variable defaults
     * @var array
     */
    public $components = [
        'Security' => array(
            'csrfExpires' => '+1 hour'
        ),
        'Flash',
        'Auth' => [
            'loginRedirect' => [
                'controller' => 'posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'register',
                /*'home'*/
            ],
            'authenticate' => [
                'Form' => [
                    'passwordHasher' => 'Blowfish',
                ]
            ],
            'authorize' => ['Controller']
        ],
        'Session',
    ];

    /**
     * Check Authorization before filter
     * if yes set me @var array to Authenticated User 
     */
    public function beforeFilter()
    {
        $title = 'Microblog 2';
        if ($this->Auth->user()) {
            $this->set(['me' => $this->Auth->user(), 'title' => $title]);
        } else {
            $this->set(['title' => $title]);
        }
        $this->Security->blackHoleCallback = 'blackhole';
    }

    /**
     * Check Authorization before render
     * set validations @var array to PostValidation Message or CommentValidation Message
     *  if there is any or just blank if none   
     */
    public function beforeRender()
    {
        if ($this->Auth->user()) {
            $validations = '';

            //Check if there is Validation Error in Post
            if ($this->Session->check('PostValidation')) {
                $validations .= $this->Session->read('PostValidation.post.0');
                $this->Session->delete('PostValidation');
            }

            //Check if there is Validtaion Error in Comment
            if ($this->Session->check('CommentValidation')) {
                $validations = $this->Session->read('CommentValidation.comment.0');
                $this->Session->delete('CommentValidation');
            }

            //Check if there is Validation Error in Changing Password
            if ($this->Session->check('PasswordValidation')) {
                if ($this->Session->check('PasswordValidation.password')) {
                    $validations .= $this->Session->read('PasswordValidation.password.0') . '<br/>';
                }
                if ($this->Session->check('PasswordValidation.confirm_password')) {
                    $validations .= $this->Session->read('PasswordValidation.confirm_password.0') . '<br/>';
                }
                $this->Session->delete('PasswordValidation');
            }

            //Check if there is Validation Error in Changing Username or Email
            if ($this->Session->check('UsernameAndEmailValidation')) {
                if ($this->Session->check('UsernameAndEmailValidation.username')) {
                    $validations .= $this->Session->read('UsernameAndEmailValidation.username.0') . '<br/>';
                }
                if ($this->Session->check('UsernameAndEmailValidation.email')) {
                    $validations .= $this->Session->read('UsernameAndEmailValidation.email.0') . '<br/>';
                }
                $this->Session->delete('UsernameAndEmailValidation');
            }

            $this->set(['validations' => $validations]);
        }
    }

    /**
     * 
     * return json response of the data   
     */
    public function json($data)
    {
        $this->response->body(json_encode($data));
        return $this->response;
    }

    /**
     * 
     * for security
     */
    public function blackhole($type)
    {
        $this->Flash->error(
            __('Stop Illegal User Action in ' . $type . '.')
        );
        return $this->redirect(Router::url($this->referer(), true));
    }
}
