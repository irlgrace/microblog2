<?php
class LikesController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool
     */
    public function isAuthorized($user)
    {   //like routes is avilable for everyone
        if (in_array($this->action, ['like'])) {
            return true;
        }
    }

    /**
     * Set layout and unlockedAction before filter 
     *  
     * @return bool 
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = ['like'];
        $this->layout = 'inside';
    }

    /**
     * Like or unlike a post
     * 
     * @return json response or null
     */
    public function like()
    {
        if ($this->request->is(['post'])) {
            $this->autoRender = false;

            if (isset($this->request->data['post_id'])) {
                $this->loadModel('Post');

                //Check if post is existing and not deleted
                if ($this->Post->isInvalid($this->request->data['post_id'])) {
                    return null;
                }

                $conditions = [
                    'Like.user_id' => $this->Auth->user('id'),
                    'Like.post_id' => $this->request->data['post_id']
                ];

                $saved = false;

                //Check if the record is existing record
                if ($this->Like->hasAny($conditions)) {
                    $this->Like->contain();
                    $like = $this->Like->find('first', ['conditions' => $conditions]);
                    if ($like['Like']['deleted']) {
                        $like['Like']['deleted'] = false;
                    } else {
                        $like['Like']['deleted'] = true;
                    }

                    if ($this->Like->save($like)) {
                        $saved = true;
                    }
                } else {
                    //Create new record if not existing
                    $this->Like->create();
                    $like['Like']['user_id'] = $this->Auth->user('id');
                    $like['Like']['post_id'] = $this->request->data['post_id'];
                    $like['Like']['deleted'] = false;

                    if ($this->Like->save($like)) {
                        $saved = true;
                    }
                }

                if ($saved) { //if saved successfully
                    $this->Post->contain();

                    //Getting the updated post like count
                    $postLikeCount = $this->Post->read('like_count', $this->request->data['post_id']);
                    $like['Like']['like_count'] = $postLikeCount['Post']['like_count'];

                    return $this->json($like['Like']);
                }
            }
            
            return null;
        }
    }
}
