<?php
class CommentsController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool or @return redirected page
     */
    public function isAuthorized($user)
    {
        //Anyone that are login can access this action
        if (in_array($this->action, ['add', 'seeMore', 'index', 'fetchComment', 'edit'])) {
            return true;
        }

        /*If the comment is owned by authenticated user, 
        it can access the action otherwise return to previous page*/
        if (in_array($this->action, ['editPost', 'delete'])) {

            $commentId = (int) $this->request->params['pass'][0];

            if ($this->Comment->isOwnedBy($commentId, $user['id'])) {
                return true;
            } else {
                $this->Flash->error(__('Request not Authorized'));
                return $this->redirect(Router::url($this->referer(), true));
            }
        }
    }

    /**
     * Set layout and filter actions before filter 
     *  
     * @return bool or @return redirected page
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'inside';

        $this->Security->unlockedActions = ['fetchComment'];

        //In this actions, check if it is accessing a existing and not deleted comment
        if (in_array($this->action, ['editPost', 'delete'])) {

            //Check if accessing the right url with parameter
            if (empty($this->request->params['pass'])) {
                $this->Flash->error(__('No comment referencing'));
                return $this->redirect(Router::url($this->referer(), true));
            }

            $commentId = (int) $this->request->params['pass'][0];

            if ($this->Comment->isInvalid($commentId)) {
                $this->Flash->error(__('Comment does not exist'));
                return $this->redirect(Router::url($this->referer(), true));
            }
        }
        return true;
    }

    /**
     * Function for Adding comment
     * 
     * @return redirected page
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Comment->create();
            $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
            $this->request->data['Comment']['comment'] = trim($this->request->data['Comment']['comment']);
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Successfully commented in a post.'));
            } else {
                /*Set a session named CommentValidation if an error occured upon saving
                This is retrieve in AppController beforeRender function*/
                $this->Session->write('CommentValidation', $this->Comment->validationErrors);
            }
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving Editing Comment Form
     * 
     * @return ajax view or null
     * @throws NotFoundException
     * @param $id or null
     */
    public function edit($id = null)
    {
        $this->layout = 'ajax';
        try {
            if (!$id) {
                throw new NotFoundException(__('No Comment Referencing'));
            }

            if ($this->Comment->isInvalid($id)) {
                throw new NotFoundException(__('Invalid Comment'));
            }

            if (!$this->Comment->isOwnedBy($id, $this->Auth->user('id'))) {
                throw new NotFoundException(__('Invalid Request'));
            }

            $comment = $this->Comment->findById($id);
            if (!$comment) {
                throw new NotFoundException(__('Invalid comment'));
            }
            if (!$this->request->data) {
                $this->request->data = $comment;
            }
        } catch (NotFoundException $e) {
            $this->set('error', $e->getMessage());
        }
    }

    /**
     * Function for Editing Comment submitted through post or put
     * 
     * @return redirected page with flash message
     * @throws NotFoundException
     * @param $id or null
     */
    public function editPost($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('Invalid comment'));
            }
            $this->autoRender = false;
            if ($this->request->is(['post', 'put'])) {
                $this->Comment->read('comment', $id);
                $this->Comment->set(['comment' => trim($this->request->data['Comment']['comment'])]);
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Comment has been updated.'));
                } else {
                    /*Set a session named CommentValidation if an error occured upon saving
                    This is retrieve in AppController beforeRender function*/
                    $this->Session->write('CommentValidation', $this->Comment->validationErrors);
                }
            }
        } catch (NotFoundException $e) {
            $this->Flash->error(_($e->getMessage()));
        } finally {
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Viewing all Comments for specific post
     * 
     * @return view or redirected page
     * @throws NotFoundException
     * @param null or $id of post
     */
    public function seeMore($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No post refencing'));
            }
            $this->loadModel('Post');

            if ($this->Post->isInvalid($id)) { //Check if post is existing and not deleted
                throw new NotFoundException(__('Post does not exist'));
            }

            $this->Post->contain(
                [
                    'User.username',
                    'User.image',
                    'RetweetedPost.post',
                    'RetweetedPost.deleted',
                    'RetweetedPost.post_image',
                    'RetweetedPost.User',
                    'Like.user_id =' . $this->Auth->user('id'),
                ]
            );

            $post = $this->Post->find('first', [
                'conditions' => ['Post.id' => $id, 'Post.deleted' => false]
            ]);

            $this->set(['post' => $post]);
        } catch (NotFoundException $e) {
            $this->Flash->error(_($e->getMessage()));
            return $this->redirect(Router::url($this->referer(), true));
        }
    }

    /**
     * Function for Retrieving all Comments for specific post
     * 
     * @return ajax view
     */
    public function fetchComment()
    {
        $this->layout = 'ajax';
        if ($this->request->is(['post', 'ajax'])) {
            if (
                isset($this->request->data['limit']) &&
                isset($this->request->data['page']) &&
                isset($this->request->data['post_id'])
            ) {
                $comments = $this->Comment->fetchComment(
                    $this->request->data['post_id'],
                    $this->request->data['limit'],
                    $this->request->data['page'],
                );

                $this->set(['comments' => $comments, 'page' => $this->request->data['page']]);
            }
        }
    }

    /**
     * Function for Soft deleting a comment
     * 
     * @return redirected page with flash message
     * @throws MethodNotAllowedException
     * @param $id
     */
    public function delete($id)
    {
        try {
            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $this->Comment->read(['deleted', 'deleted_date'], $id);
            $this->Comment->set([
                'deleted_date' => date('Y-m-d H:i:s'),
                'deleted' => true
            ]);

            if ($this->Comment->save()) {
                $this->Flash->success(
                    __('Successfully deleted a post!')
                );
            } else {
                $this->Flash->error(
                    __('Error while deleting a post.')
                );
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(
                __('Access not authorized.')
            );
        } finally {
            return $this->redirect(Router::url($this->referer(), true));
        }
    }
}
