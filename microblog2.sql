-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2020 at 05:05 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog2`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(140) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `post_id`, `created`, `modified`, `deleted_date`, `deleted`) VALUES
(1, 'same here', 1, 1, '2020-02-05 08:54:35', '2020-02-05 08:54:35', NULL, 0),
(2, 'Hi! Good Afternoon', 2, 3, '2020-02-05 08:58:49', '2020-02-05 08:58:49', NULL, 0),
(3, 'Hi! Good Afternoon', 2, 3, '2020-02-05 08:58:49', '2020-02-05 08:58:58', '2020-02-05 08:58:58', 1),
(4, 'Hi!', 2, 2, '2020-02-05 08:59:09', '2020-02-05 08:59:09', NULL, 0),
(5, 'Arf! Arf!', 5, 2, '2020-02-05 09:03:46', '2020-02-05 09:03:46', NULL, 0),
(6, 'comment', 2, 15, '2020-02-05 09:33:13', '2020-02-05 09:33:13', NULL, 0),
(7, 'comment', 1, 18, '2020-02-06 01:54:30', '2020-02-06 01:54:30', NULL, 0),
(8, 'comment ako hehe', 1, 19, '2020-02-06 03:19:37', '2020-02-06 03:20:40', NULL, 0),
(9, 'comment1', 1, 22, '2020-02-06 05:09:39', '2020-02-06 05:09:39', NULL, 0),
(10, 'comment2', 1, 22, '2020-02-06 05:09:46', '2020-02-06 05:09:46', NULL, 0),
(11, 'comment3', 1, 22, '2020-02-06 05:09:55', '2020-02-06 05:09:55', NULL, 0),
(12, 'comment4', 1, 22, '2020-02-06 05:10:11', '2020-02-06 05:10:11', NULL, 0),
(13, 'nylle1 edit', 1, 20, '2020-02-06 05:10:41', '2020-02-06 05:11:57', NULL, 0),
(14, 'nylle2', 1, 20, '2020-02-06 05:10:54', '2020-02-06 05:10:54', NULL, 0),
(15, 'nylle3 edit', 1, 20, '2020-02-06 05:11:04', '2020-02-06 05:12:10', NULL, 0),
(16, 'nylle4', 1, 20, '2020-02-06 05:11:19', '2020-02-06 05:11:19', NULL, 0),
(17, 'nylle5 edit', 1, 20, '2020-02-06 05:11:36', '2020-02-06 05:12:22', NULL, 0),
(18, 'bat removed?', 1, 23, '2020-02-06 05:14:21', '2020-02-06 05:14:21', NULL, 0),
(19, 'comment 1 edit ko', 1, 26, '2020-02-06 05:16:46', '2020-02-06 05:59:03', NULL, 0),
(20, 'comment 2', 9, 26, '2020-02-06 05:17:24', '2020-02-06 05:17:24', NULL, 0),
(21, 'jkaJ ', 9, 26, '2020-02-06 05:17:36', '2020-02-06 05:32:47', NULL, 0),
(22, 'comment 4', 1, 26, '2020-02-06 05:17:57', '2020-02-06 05:17:57', NULL, 0),
(23, 'comment 5 fdffffffffffffffffffsdf sdsf s gf g ghhffh fghfh ghgh ghfgh ghfghf hfg hfg gfh hjhsh hjhsdja hjdhjasdha jhsdjhjkahjdhka hdjhajhdkj', 9, 26, '2020-02-06 05:27:11', '2020-02-06 05:28:11', NULL, 0),
(24, 'hey', 1, 4, '2020-02-06 06:18:02', '2020-02-06 06:18:02', NULL, 0),
(25, 'comment ako', 2, 40, '2020-02-06 08:23:57', '2020-02-06 08:23:57', NULL, 0),
(26, 'ay walang comment', 2, 38, '2020-02-06 08:25:45', '2020-02-06 08:25:45', NULL, 0),
(27, 'yas', 1, 27, '2020-02-06 09:25:19', '2020-02-06 09:25:19', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `following_user_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `following_user_id`, `created`, `modified`, `deleted`) VALUES
(1, 2, 1, '2020-02-05 08:58:04', '2020-02-05 08:58:04', 0),
(2, 2, 4, '2020-02-05 08:58:12', '2020-02-05 08:58:12', 0),
(3, 5, 2, '2020-02-05 09:03:04', '2020-02-05 09:03:04', 0),
(4, 5, 4, '2020-02-05 09:03:09', '2020-02-05 09:03:09', 0),
(5, 5, 1, '2020-02-05 09:03:20', '2020-02-05 09:03:20', 0),
(6, 6, 1, '2020-02-05 09:06:59', '2020-02-05 09:06:59', 0),
(7, 6, 5, '2020-02-05 09:07:14', '2020-02-05 09:07:14', 0),
(8, 7, 1, '2020-02-05 09:09:15', '2020-02-05 09:09:15', 0),
(9, 7, 5, '2020-02-05 09:09:26', '2020-02-05 09:09:26', 0),
(10, 7, 2, '2020-02-05 09:09:34', '2020-02-05 09:09:34', 0),
(11, 7, 4, '2020-02-05 09:09:41', '2020-02-05 09:09:41', 0),
(12, 7, 3, '2020-02-05 09:09:48', '2020-02-05 09:09:48', 0),
(13, 8, 1, '2020-02-05 09:12:04', '2020-02-05 09:12:04', 0),
(14, 8, 7, '2020-02-05 09:12:09', '2020-02-05 09:12:09', 0),
(15, 8, 5, '2020-02-05 09:12:14', '2020-02-05 09:12:14', 0),
(16, 8, 2, '2020-02-05 09:12:19', '2020-02-05 09:12:19', 0),
(17, 8, 4, '2020-02-05 09:12:24', '2020-02-05 09:12:24', 0),
(18, 8, 3, '2020-02-05 09:12:45', '2020-02-05 09:12:45', 0),
(19, 1, 3, '2020-02-05 09:13:16', '2020-02-05 09:13:16', 0),
(20, 1, 4, '2020-02-05 09:13:36', '2020-02-05 09:13:36', 0),
(21, 1, 2, '2020-02-05 09:13:48', '2020-02-05 09:13:48', 0),
(22, 1, 8, '2020-02-05 09:15:07', '2020-02-05 09:15:07', 0),
(23, 1, 7, '2020-02-06 02:53:59', '2020-02-06 02:53:59', 0),
(24, 9, 1, '2020-02-06 04:55:52', '2020-02-06 04:55:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `created`, `modified`, `deleted`) VALUES
(1, 1, 1, '2020-02-05 08:54:29', '2020-02-05 08:54:29', 0),
(2, 3, 2, '2020-02-05 08:57:55', '2020-02-05 08:57:55', 0),
(3, 2, 2, '2020-02-05 08:57:57', '2020-02-05 08:57:57', 0),
(4, 1, 2, '2020-02-05 08:58:00', '2020-02-05 08:58:00', 0),
(5, 4, 5, '2020-02-05 09:02:05', '2020-02-05 09:02:05', 0),
(6, 5, 5, '2020-02-05 09:02:57', '2020-02-05 09:02:57', 0),
(7, 2, 7, '2020-02-05 09:09:11', '2020-02-05 09:09:11', 0),
(8, 1, 7, '2020-02-05 09:09:12', '2020-02-05 09:09:12', 0),
(9, 3, 7, '2020-02-05 09:10:18', '2020-02-05 09:10:18', 0),
(10, 4, 7, '2020-02-05 09:10:20', '2020-02-05 09:10:20', 0),
(11, 5, 7, '2020-02-05 09:10:21', '2020-02-05 09:10:21', 0),
(12, 6, 7, '2020-02-05 09:10:23', '2020-02-05 09:10:23', 0),
(13, 1, 8, '2020-02-05 09:11:57', '2020-02-05 09:11:57', 0),
(14, 2, 8, '2020-02-05 09:11:59', '2020-02-05 09:11:59', 0),
(15, 3, 8, '2020-02-05 09:12:33', '2020-02-05 09:12:33', 0),
(16, 5, 8, '2020-02-05 09:14:49', '2020-02-05 09:14:49', 0),
(17, 4, 8, '2020-02-05 09:14:53', '2020-02-05 09:14:53', 0),
(18, 17, 1, '2020-02-06 01:56:46', '2020-02-06 01:56:46', 0),
(19, 18, 1, '2020-02-06 01:59:45', '2020-02-06 01:59:45', 1),
(20, 19, 1, '2020-02-06 02:42:39', '2020-02-06 02:42:39', 0),
(21, 20, 9, '2020-02-06 03:51:59', '2020-02-06 03:51:59', 0),
(22, 23, 1, '2020-02-06 05:14:30', '2020-02-06 05:14:30', 0),
(23, 24, 1, '2020-02-06 05:15:36', '2020-02-06 05:15:36', 0),
(24, 25, 1, '2020-02-06 05:15:38', '2020-02-06 05:15:38', 0),
(25, 26, 9, '2020-02-06 05:17:15', '2020-02-06 05:17:15', 0),
(26, 27, 1, '2020-02-06 05:47:46', '2020-02-06 05:47:46', 0),
(27, 14, 1, '2020-02-06 06:05:09', '2020-02-06 06:05:09', 0),
(28, 28, 1, '2020-02-06 06:05:16', '2020-02-06 06:05:16', 0),
(29, 4, 1, '2020-02-06 06:17:56', '2020-02-06 06:17:56', 0),
(30, 40, 2, '2020-02-06 08:23:49', '2020-02-06 08:23:49', 0),
(31, 31, 2, '2020-02-06 08:24:24', '2020-02-06 08:24:24', 0),
(32, 32, 2, '2020-02-06 08:24:25', '2020-02-06 08:24:25', 0),
(33, 33, 2, '2020-02-06 08:24:27', '2020-02-06 08:24:27', 0),
(34, 30, 2, '2020-02-06 08:24:31', '2020-02-06 08:24:31', 0),
(35, 34, 2, '2020-02-06 08:24:38', '2020-02-06 08:24:38', 0),
(36, 35, 2, '2020-02-06 08:24:39', '2020-02-06 08:24:39', 0),
(37, 36, 2, '2020-02-06 08:24:40', '2020-02-06 08:24:40', 0),
(38, 41, 2, '2020-02-06 08:24:46', '2020-02-06 08:24:46', 0),
(39, 14, 2, '2020-02-06 08:24:54', '2020-02-06 08:24:54', 0),
(40, 38, 2, '2020-02-06 08:25:03', '2020-02-06 08:25:03', 0),
(41, 12, 2, '2020-02-06 08:28:53', '2020-02-06 08:28:53', 0),
(42, 13, 2, '2020-02-06 08:28:57', '2020-02-06 08:28:57', 0),
(43, 10, 2, '2020-02-06 08:28:58', '2020-02-06 08:28:58', 0),
(44, 9, 2, '2020-02-06 08:29:00', '2020-02-06 08:29:00', 0),
(45, 8, 2, '2020-02-06 08:29:02', '2020-02-06 08:29:02', 0),
(46, 7, 2, '2020-02-06 08:29:04', '2020-02-06 08:29:04', 0),
(47, 6, 2, '2020-02-06 08:29:05', '2020-02-06 08:29:05', 0),
(48, 39, 2, '2020-02-06 08:32:28', '2020-02-06 08:32:28', 0),
(49, 11, 1, '2020-02-06 09:25:04', '2020-02-06 09:25:04', 0),
(50, 15, 1, '2020-02-07 02:41:22', '2020-02-07 02:41:22', 0),
(51, 16, 1, '2020-02-07 02:41:24', '2020-02-07 02:41:24', 0),
(52, 30, 1, '2020-02-07 02:48:21', '2020-02-07 02:48:21', 0),
(53, 31, 1, '2020-02-07 02:48:23', '2020-02-07 02:48:23', 0),
(54, 43, 1, '2020-02-07 03:50:37', '2020-02-07 03:50:37', 0),
(55, 42, 1, '2020-02-07 03:50:39', '2020-02-07 03:50:39', 0),
(56, 38, 1, '2020-02-07 03:50:45', '2020-02-07 03:50:45', 0),
(57, 26, 1, '2020-02-07 04:03:56', '2020-02-07 04:03:56', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post` varchar(140) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `retweeted_post_id` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `comment_count` int(11) NOT NULL DEFAULT 0,
  `like_count` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post`, `user_id`, `retweeted_post_id`, `created`, `modified`, `deleted_date`, `deleted`, `comment_count`, `like_count`) VALUES
(1, 'hi? newbie here', 3, NULL, '2020-02-05 08:53:42', '2020-02-05 08:53:42', NULL, 0, 1, 4),
(2, 'First time posting hereee........', 1, NULL, '2020-02-05 08:54:20', '2020-02-05 08:54:20', NULL, 0, 2, 3),
(3, 'Hello. Good Afternoon.', 4, NULL, '2020-02-05 08:57:34', '2020-02-05 08:57:34', NULL, 0, 1, 3),
(4, 'Hey!', 2, NULL, '2020-02-05 08:58:33', '2020-02-05 08:58:33', NULL, 0, 1, 4),
(5, 'Arf! Arf!', 5, NULL, '2020-02-05 09:02:17', '2020-02-05 09:02:17', NULL, 0, 0, 3),
(6, 'Alpha', 7, NULL, '2020-02-05 09:09:03', '2020-02-05 09:09:03', NULL, 0, 0, 2),
(7, 'Aw. Aw. Aw.', 8, NULL, '2020-02-05 09:14:05', '2020-02-05 09:14:05', NULL, 0, 0, 1),
(8, 'Aw. Aw. Aw.', 8, 5, '2020-02-05 09:14:34', '2020-02-05 09:14:34', NULL, 0, 0, 1),
(9, 'â€œBe yourself; everyone else is already taken.â€\r\nâ€• Oscar Wilde', 1, NULL, '2020-02-05 09:16:13', '2020-02-05 09:16:13', NULL, 0, 0, 1),
(10, 'â€œTwo things are infinite: the universe and human stupidity; and I\'m not sure about the universe.â€\r\nâ€• Albert Einstein', 8, NULL, '2020-02-05 09:16:38', '2020-02-05 09:16:38', NULL, 0, 0, 1),
(11, 'â€œSo many books, so little time.â€\r\nâ€• Frank Zappa', 1, NULL, '2020-02-05 09:17:15', '2020-02-05 09:17:15', NULL, 0, 0, 1),
(12, 'â€œYou know you\'re in love when you can\'t fall asleep because reality is finally better than your dreams.â€\r\nâ€• Dr. Seuss\r\n', 1, NULL, '2020-02-05 09:17:58', '2020-02-05 09:17:58', NULL, 0, 0, 1),
(13, 'â€œYou only live once, but if you do it right, once is enough.â€\r\nâ€• Mae West', 8, NULL, '2020-02-05 09:18:28', '2020-02-05 09:18:28', NULL, 0, 0, 1),
(14, 'â€œBe the change that you wish to see in the world.â€\r\nâ€• Mahatma Gandhi', 2, NULL, '2020-02-05 09:19:22', '2020-02-05 09:19:22', NULL, 0, 0, 2),
(15, 'ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', 2, NULL, '2020-02-05 09:20:31', '2020-02-05 09:20:31', NULL, 0, 1, 1),
(16, 'ass                      aaa', 2, NULL, '2020-02-05 09:33:31', '2020-02-05 09:33:31', NULL, 0, 0, 1),
(17, 'â€œIf you tell the truth, you don\'t have to remember anything.â€\r\nâ€• Mark Twain', 2, NULL, '2020-02-05 09:35:00', '2020-02-05 09:35:00', NULL, 0, 0, 1),
(18, 'fgdgs hehe edited', 1, NULL, '2020-02-06 01:54:01', '2020-02-06 06:05:37', '2020-02-06 06:05:37', 1, 1, 0),
(19, 'hey?', 1, NULL, '2020-02-06 02:17:52', '2020-02-06 02:17:52', NULL, 0, 1, 1),
(20, 'nylle :)', 1, NULL, '2020-02-06 03:20:59', '2020-02-06 05:12:53', '2020-02-06 05:12:53', 1, 5, 1),
(21, 'hehe', 9, NULL, '2020-02-06 04:50:55', '2020-02-06 04:54:54', '2020-02-06 04:54:54', 1, 0, 0),
(22, 'hey', 1, NULL, '2020-02-06 05:09:03', '2020-02-06 05:49:18', '2020-02-06 05:49:18', 1, 4, 0),
(23, 'retweet ko si Nylle', 1, 20, '2020-02-06 05:12:41', '2020-02-06 05:14:39', '2020-02-06 05:14:39', 1, 1, 1),
(24, 'post from home page', 1, NULL, '2020-02-06 05:15:09', '2020-02-06 05:15:09', NULL, 0, 0, 1),
(25, 'post from profile', 1, NULL, '2020-02-06 05:15:30', '2020-02-06 05:15:30', NULL, 0, 0, 1),
(26, 'For Comment More than 3', 1, NULL, '2020-02-06 05:16:18', '2020-02-06 05:17:06', NULL, 0, 5, 2),
(27, 'hey shdjhsj hsajhdj hsdjh hsjdhaj jshjdahjds ajhsjdhajhdj jshdjahjsdh jshjdhjash jhdsjahdjhj djhsdjhasj djshdjhsj hdjashdj hasjdhajshd hsdja', 9, NULL, '2020-02-06 05:36:36', '2020-02-06 05:36:36', NULL, 0, 1, 1),
(28, 'hehe', 1, 18, '2020-02-06 05:49:43', '2020-02-06 05:49:43', NULL, 0, 0, 1),
(29, 'retweet again. lol', 1, 18, '2020-02-06 06:05:31', '2020-02-06 06:15:08', NULL, 0, 0, 0),
(30, 'luh hehe nays... no warning', 1, NULL, '2020-02-06 06:45:37', '2020-02-06 06:56:48', NULL, 0, 0, 2),
(31, 'yey', 1, NULL, '2020-02-06 06:59:05', '2020-02-06 06:59:05', NULL, 0, 0, 2),
(32, 'no warning', 1, NULL, '2020-02-06 06:59:13', '2020-02-06 06:59:13', NULL, 0, 0, 1),
(33, 'meron eh huhuhu', 1, NULL, '2020-02-06 07:08:14', '2020-02-06 07:08:14', NULL, 0, 0, 1),
(34, 'bat kaya?', 1, NULL, '2020-02-06 07:08:36', '2020-02-06 07:08:36', NULL, 0, 0, 1),
(35, 'lalalala', 1, NULL, '2020-02-06 07:08:41', '2020-02-06 07:08:41', NULL, 0, 0, 1),
(36, 'errroooor', 1, NULL, '2020-02-06 07:20:36', '2020-02-06 07:20:36', NULL, 0, 0, 1),
(37, 'ghad hahah', 1, NULL, '2020-02-06 08:19:41', '2020-02-06 08:21:58', '2020-02-06 08:21:58', 1, 0, 0),
(38, 'access denied? lol', 1, NULL, '2020-02-06 08:19:53', '2020-02-06 08:19:53', NULL, 0, 1, 2),
(39, 'no warning naaa yey', 1, NULL, '2020-02-06 08:21:19', '2020-02-06 08:21:19', NULL, 0, 0, 1),
(40, 'yey', 1, NULL, '2020-02-06 08:23:29', '2020-02-07 02:41:46', '2020-02-07 02:41:46', 1, 1, 1),
(41, 'retweet ko', 2, 40, '2020-02-06 08:24:12', '2020-02-06 08:24:12', NULL, 0, 0, 1),
(42, 'retweet ko yey', 1, 40, '2020-02-06 09:13:14', '2020-02-07 02:48:36', NULL, 0, 0, 1),
(43, 'yey?', 2, NULL, '2020-02-07 03:35:04', '2020-02-07 03:35:04', NULL, 0, 0, 1),
(44, 'yey?', 1, 43, '2020-02-07 03:53:06', '2020-02-07 03:53:06', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 0,
  `activation_code` varchar(8) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0,
  `deleted_date` datetime DEFAULT NULL,
  `follower_count` int(11) NOT NULL DEFAULT 0,
  `following_count` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `image`, `activated`, `activation_code`, `created`, `modified`, `deleted`, `deleted_date`, `follower_count`, `following_count`) VALUES
(1, 'grace', 'irlgrace@getnada.com', '$2a$10$r8a9zuDIf0grFth5dbkuN.gkLji4Nf0jepOnvCPrgGuMzWYYYt4c.', '/img/user/c4ca4238a0b923820dcc509a6f75849b.jpg', 1, '1b1fd975', '2020-02-05 08:46:29', '2020-02-07 03:51:01', 0, NULL, 6, 5),
(2, 'nylle17', 'nylle@getnada.com', '$2a$10$b9gx.wB6Q/DsKrIwzs1yDOJMrXfX7mQVapNDOBIgWWbEb0c/8Q7Xq', '/img/user/c81e728d9d4c2f636f067f89cc14862c.png', 1, '86f22e6b', '2020-02-05 08:49:29', '2020-02-07 03:50:18', 0, NULL, 4, 2),
(3, 'nylle_brian', 'nylle.brian@getnada.com', '$2a$10$BNxXZETi.yMYMGt.hL6n3.a6f7WXBz/Jy3ZKlnh/7QtTlvorxVyFa', NULL, 1, 'eb5feff6', '2020-02-05 08:53:02', '2020-02-05 08:53:26', 0, NULL, 3, 0),
(4, 'irl_grace', 'irl.grace@getnada.com', '$2a$10$xpo1CalJNxD7UOCklsen0u.VLKSxkoAAxniNKK5SeOBta8zhmSYI.', NULL, 1, '556b2530', '2020-02-05 08:56:51', '2020-02-05 08:57:14', 0, NULL, 5, 0),
(5, 'bravo', 'bravo.lab@getnada.com', '$2a$10$gQVIYgVgutDgqzfzX.zLEej/gloGrsoUx/Wnr27z2V5VLb8ETFt52', '/img/user/e4da3b7fbbce2345d7772b0674a318d5.jpg', 1, '989a462d', '2020-02-05 09:01:26', '2020-02-05 09:02:39', 0, NULL, 3, 3),
(6, 'someone', 'zyqosexa@getnada.com', '$2a$10$/pnlTtU7OL.FmHwuYfiTEu3YFw7N303gV9FtihX6LzrldRMru46tC', NULL, 1, '51420855', '2020-02-05 09:06:24', '2020-02-05 09:06:38', 0, NULL, 0, 2),
(7, 'alpha', 'alpha.lab@getnada.com', '$2a$10$gpTawELTtKIVVWuuCBqfo.G8PumGUuVHRFp7JVT58B8991OZWvBe2', NULL, 1, 'b22fcb38', '2020-02-05 09:08:36', '2020-02-05 09:08:53', 0, NULL, 2, 5),
(8, 'krissy_brown', 'krissy.brown@getnada.com', '$2a$10$4e6DA/HQ6i6WC05eTumNGulsE6hU/c09OZ1M8ReYis6BnGtbzJAW.', NULL, 1, 'f1655812', '2020-02-05 09:11:41', '2020-02-05 09:11:51', 0, NULL, 1, 6),
(9, 'sample', 'sample.test@getnada.com', '$2a$10$kncNqFgwDlsPERhQCB06Ju4R46yWV5uk3v9.FxUxhQAOMsdPIF1He', NULL, 1, 'b74bf08c', '2020-02-06 03:51:09', '2020-02-06 03:51:41', 0, NULL, 0, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
